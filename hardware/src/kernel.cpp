/*
 *
 * Xilinx Open Hardware Contest: June 30, 2018
 *
 * kernel.cpp  
 *
 * Author:
 * Filippo Carloni:    filippo.carloni@mail.polimi.it
 * Giada Casagrande:   giada.casagrande@mail.polimi.it
 * Valentina Corbetta: valentina.corbetta@mail.polimi.it
 *
 */




#define PIPELINE 8
#define PARTIAL 4
#define ACC_VAL_SIZE 16
#define PARTIAL_LOADING 42
#define NCOMP 84
#define DIV_NCOMP 0.0119047619// 1.0f/NCOMP
#define NMAX 150  
#define TOLL 0.0001
#define COUNT 42464 

#include "kernel.hpp"
#include <math.h>
#include <string.h>
#include <stdio.h>

void zeros_vector(my_double mat[]); //Function zeros_vector is named zeros_vet in FastBRaIn_Report
void vector_extraction(my_double vett[], my_int r_vett, my_double mat[][NCOMP], my_int ind); //Function vector_extraction is named vett_from_mat in FastBRaIn_Report 
void f1_function(my_double a1[], my_double *a2, my_double a_mult[]); //Function f1_function is named vet_matrix_mult in FastBRaIn_Report
void W_parte1_computation(my_double *a1, my_double a2[], my_double a_mult[]); //Function W_parte1_computation is named matrix_vet_mult in FastBRaIn_Report 
my_double vectors_multiplication(my_double a1[], my_double a2[], my_int c1); //Function vectors_multiplication is named vet_vet_mult in FastBRaIn_Report 

/* synthesis help function*/
void division_by_NCOMP(my_double whiteM_div[NCOMP][COUNT],my_double whiteM[NCOMP][COUNT]);
void W_updating(my_double W[NCOMP][NCOMP], my_double W_vett[NCOMP], my_int i);
my_double addition(my_double f2_out[]);

/* depth constant*/
const int wdepth= NCOMP*NCOMP;
const int withdepth=NCOMP*COUNT;

/* 
 * non linear contrast functions needed in FastICA algorithm:
 * f1(x) = tanh(x)
 * f2(x) = 1-(tanh(x))^2 (first derivative of the above function)
 */

void kernel( my_double* Wmat, my_double *whiteMat, my_double *white_mat_t, int *failure_value){
/* Interface type declaration */

#pragma HLS INTERFACE m_axi port=whiteMat bundle=gmem0
#pragma HLS INTERFACE m_axi port=Wmat bundle=gmem0
#pragma HLS INTERFACE m_axi port=white_mat_t bundle=gmem0
#pragma HLS INTERFACE m_axi port=failure_value bundle=gmem1

#pragma HLS INTERFACE s_axilite port=whiteMat bundle=control
#pragma HLS INTERFACE s_axilite port=white_mat_t bundle=control
#pragma HLS INTERFACE s_axilite port=Wmat bundle=control
#pragma HLS INTERFACE s_axilite port=failure_value bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

     my_double W[NCOMP][NCOMP];
     my_int num_iter,failure;
     my_double temperr, W_p2,tmp;
     my_double wmem[NCOMP],f1_out[COUNT],W_parte1[NCOMP],f2_out[COUNT], wtemp[NCOMP], W_vett_j[NCOMP];
	 failure=0;
	 my_double W_vett[NCOMP];
	 my_double W_vett_tmp[NCOMP];

/* Array partioning to have parallel eccess */ 

#pragma HLS ARRAY_PARTITION variable=W complete dim=1
#pragma HLS ARRAY_PARTITION variable=W_vett_tmp complete dim=1
#pragma HLS ARRAY_PARTITION variable=W_vett complete dim=1
#pragma HLS ARRAY_PARTITION variable=wmem complete dim=1
#pragma HLS ARRAY_PARTITION variable=wtemp complete dim=1
#pragma HLS ARRAY_PARTITION variable=W_vett_j complete dim=1

/* Transfer of W in local memory of FPGA */

     memcpy(W,Wmat,NCOMP*NCOMP*sizeof(my_double));
	 
/* End of transfer */ 

     EXTERNAL_LOOP:
	 num_iter=0;
     for (int i=0; i<NCOMP; i++) {
	 		// variable 'temperr' is inizialized to 1 to ensure at least one iteration in the following FOR loop  
            temperr=1.0;
			// 'wmem' is inizialized to zeros
            zeros_vector(wmem);
            
			
            for(num_iter=0;num_iter<NMAX && temperr>TOLL;num_iter++) {
				
			   // The i-th column from random matrix 'W' is extracted and saved in vector 'W_vett_temp'. Then the multiplication between vector 
               // 'W_vett_temp' and matrix 'white_mat_t' is executed to obtain the output ('f1_out') of non-linear function f1
                vector_extraction(W_vett_tmp,NCOMP,W, i);
				/* loading of white_mat_t */
                f1_function(W_vett_tmp,white_mat_t, f1_out);
                
				// The matrix 'whiteMat' is divided for 'NCOMP' and then multiplied by the vector 'f1_out'. The result is the term 'W_parte1'. 
                /* loading of whiteMat */ 
				W_parte1_computation(whiteMat,f1_out,W_parte1);  
				
				FOR_1:
				// The output ('f2_out') of non-linear function f2 is obtained by substracting from 1 the square of 'f1_out', then difference is divided by 'NCOMP' 
				for (int x=0;x<COUNT; x++){
					#pragma HLS PIPELINE
					my_double tmp_f1_out = (f1_out[x]*f1_out[x]);
					my_double tmp_f2=1.0F - tmp_f1_out;
					f2_out[x]=  tmp_f2 * DIV_NCOMP;
				}
				
			    // 'f2_out' elements are all added up and the result is the number 'W_p2'. Result of multiplication between this number
                // and vector 'W_vett_tmp' is substracted from 'W_parte1'
			    W_p2=addition(f2_out);
				for (int x=0;x<NCOMP; x++){
					#pragma HLS PIPELINE
					tmp=W_vett_tmp[x]*W_p2;
					W_vett[x]=W_parte1[x] -tmp;
				}

				zeros_vector(wtemp);
				
				/* To prevent different vectors from converging to the same maxima we must decorrelate the outputs after every iteration.
            	A way of achieving decorrelation is a deflation scheme based on a Gram-Schmidt-like decorrelation: this means that we estimate 
            	the independent components one by one. When we have estimated i independent components, or i vectors w1 ,..., wi (that are the
            	columns of matrix 'W'), we run the one-unit fixed-point algorithm for wi+1 , and after every iteration step it is necessary to
            	subtract from wi+1 the “projections” wTi+1 * wj * wj, j = 1 ,..., i of the previously estimated i vectors, and then renormalize wi+1:
            	1. Let: wi+1 = wi+1 − ∑ (wTi+1 * wj *wj), j=1,...,i
            	2. Let: wi+1 = wi+1 / sqrt(wTi+1 * wi+1).
            	*/ 
				  FOR_7:
				  for (int j=0; j<i; j++) {
					#pragma HLS PIPELINE
					  vector_extraction(W_vett_j,NCOMP,W, j);  
					  W_p2=vectors_multiplication( W_vett, W_vett_j, NCOMP);
					  FOR_7_internal:
					  for (int x=0;x<NCOMP; x++)
						  wtemp[x]=wtemp[x]+ W_vett_j[x]*W_p2;
				  }
				  FOR_8:
				  for (int x=0;x<NCOMP; x++)
					  #pragma HLS UNROLL
					  W_vett_tmp[x]=W_vett[x]- wtemp[x];
                  /*Orthonormalization of Gram-Schmit: it is an algorithm which helps to obtain a number of orthogonal
            	  vectors starting from a generic number of linearly independent vectors*/	
				  W_p2=vectors_multiplication(W_vett_tmp, W_vett_tmp, NCOMP);
				  W_p2=(my_double)sqrtf(W_p2);
				  FOR_9:
				  for (int x=0;x<NCOMP; x++)
					  #pragma HLS UNROLL
					  W_vett[x]=W_vett_tmp[x]/W_p2;  //column vector 'W_vett' has been decorrelated
					  
				  // Convergence error is calculated and saved in 'temperr' variable   
				  W_p2=vectors_multiplication( wmem, W_vett, NCOMP);
				  temperr=1.0-fabsf(W_p2);
				  
				  // column vector 'W_vett' is saved in 'wmem'
				  for(int x = 0; x < NCOMP; x++){
				#pragma HLS PIPELINE
					  wmem[x] = W_vett[x];
				  }
				  
				  // The old i-th column vector of matrix 'W' is overwritten with vector 'W_vett': i-th independent component has been calculated. 
            	  W_updating(W,W_vett,i);
           }
		   
		   // 'failure' variable, at first inizialized to 0, changes to 1 if convergence is not reached in a number of iterations that is less than the maximum one 
           // defined as NMAX
            if(num_iter >= NMAX)
            	failure=1;
     }
/* Transfer of W in output direction and write of the exit status */

     memcpy(Wmat,W,NCOMP*NCOMP*sizeof(my_double));
     *failure_value=failure;
}

void zeros_vector(my_double mat[]) {
    my_int i;
    ZEROS_VECTOR:
	/* initializatio  of array in completely parallel way */ 
    for (i=0;i<NCOMP;i++)
        #pragma HLS UNROLL
		mat[i]=0.0;
}

void vector_extraction(my_double vett[], my_int r_vett, my_double mat[][NCOMP], my_int ind){
	VECTOR_EXTRACTION:
	for (int i=0;i<r_vett;i++)
        #pragma HLS PIPELINE
        vett[i]=mat[i][ind];
}

void f1_function(my_double a1[], my_double *a2,  my_double a_mult[]){
	   my_double temp[NCOMP];
	   my_double acc_val[ACC_VAL_SIZE];
	   my_double final_acc_val[ACC_VAL_SIZE];
	   int i = 0;

	   #pragma HLS ARRAY_PARTITION variable=final_acc_val complete dim=1
	   F1_FUNCTION:
	   for(int j=0; j < COUNT*NCOMP; j++){
#pragma HLS PIPELINE
 /* Partials sum to avoid interdependency between for iterations, Delta of ACC_VALL_SIZE */
				int m = j % ACC_VAL_SIZE;
				int k = j % NCOMP;
				my_double a2_val = a2[j];
				my_double tmp_val = a1[k]*a2_val;
				if(k < ACC_VAL_SIZE){
					acc_val[m] = tmp_val;
				} else if (k >= ACC_VAL_SIZE && k < COUNT - ACC_VAL_SIZE){
					acc_val[m] += tmp_val;
				} else {
					final_acc_val[m] = acc_val[m] + tmp_val;
				}
				if(k == NCOMP - 1){
			/* Tree reduction to avoid critical path issues */
					my_double tmp_v_0 = final_acc_val[0] + final_acc_val[1];
					my_double tmp_v_1 = final_acc_val[2] + final_acc_val[3];
					my_double tmp_v_2 = final_acc_val[4] + final_acc_val[5];
					my_double tmp_v_3 = final_acc_val[6] + final_acc_val[7];
					my_double tmp_v_4 = final_acc_val[8] + final_acc_val[9];
					my_double tmp_v_5 = final_acc_val[10] + final_acc_val[11];
					my_double tmp_v_6 = final_acc_val[12] + final_acc_val[13];
					my_double tmp_v_7 = final_acc_val[14] + final_acc_val[15];

					my_double tmp_v_8 = tmp_v_0 + tmp_v_1;
					my_double tmp_v_9 = tmp_v_2 + tmp_v_3;
					my_double tmp_v_10 = tmp_v_4 + tmp_v_5;
					my_double tmp_v_11 = tmp_v_6 + tmp_v_7;

					my_double tmp_v_12 = tmp_v_8 + tmp_v_9;
					my_double tmp_v_13 = tmp_v_10 + tmp_v_11;

					my_double tmp_v_14 = tmp_v_12 + tmp_v_13;
					a_mult[i++]=tanhf(tmp_v_14);
				}
		}

}



void W_parte1_computation(my_double *a1, my_double a2[],my_double a_mult[]){
    my_double temp[PARTIAL][COUNT],tmp;
    my_double acc_val[ACC_VAL_SIZE];
    my_double final_acc_val[ACC_VAL_SIZE];
    int i = 0;
#pragma HLS ARRAY_PARTITION variable=final_acc_val complete dim=1
    W_PARTE1_COMPUTATION:for(int j=0; j<NCOMP*COUNT; j+=1/*PARTIAL*/){
#pragma HLS PIPELINE
				unsigned int m = j%ACC_VAL_SIZE;
				unsigned int k = j%COUNT;
				my_double a1_val = a1[j];
				tmp=(a1_val*DIV_NCOMP);
				my_double tmp1 = a2[k]*tmp;
/* Partials sum to avoid interdependency between for iterations, Delta of ACC_VALL_SIZE */
				if(k < ACC_VAL_SIZE){
					acc_val[m] = tmp1;
				} else if (k >= ACC_VAL_SIZE && k < COUNT - ACC_VAL_SIZE){
					acc_val[m] += tmp1;
				} else {
					final_acc_val[m] = tmp1 + acc_val[m];
				}
				if(k == COUNT - 1){
				/* Tree reduction to avoid critical path issues */
					my_double tmp_v_0 = final_acc_val[0] + final_acc_val[1];
					my_double tmp_v_1 = final_acc_val[2] + final_acc_val[3];
					my_double tmp_v_2 = final_acc_val[4] + final_acc_val[5];
					my_double tmp_v_3 = final_acc_val[6] + final_acc_val[7];
					my_double tmp_v_4 = final_acc_val[8] + final_acc_val[9];
					my_double tmp_v_5 = final_acc_val[10] + final_acc_val[11];
					my_double tmp_v_6 = final_acc_val[12] + final_acc_val[13];
					my_double tmp_v_7 = final_acc_val[14] + final_acc_val[15];

					my_double tmp_v_8 = tmp_v_0 + tmp_v_1;
					my_double tmp_v_9 = tmp_v_2 + tmp_v_3;
					my_double tmp_v_10 = tmp_v_4 + tmp_v_5;
					my_double tmp_v_11 = tmp_v_6 + tmp_v_7;

					my_double tmp_v_12 = tmp_v_8 + tmp_v_9;
					my_double tmp_v_13 = tmp_v_10 + tmp_v_11;

					my_double tmp_v_14 = tmp_v_12 + tmp_v_13;
			 	a_mult[i++] = tmp_v_14;
				
				}
			}
			
    }


my_double vectors_multiplication(my_double a1[], my_double a2[], my_int c1){

	my_double W_p2=0;

	my_double acc_val[ACC_VAL_SIZE];

	for(int i=0;i<c1;i++){
	/* Partials sum to avoid interdependency between for iterations, Delta of ACC_VALL_SIZE */
#pragma HLS PIPELINE
		int m = i % ACC_VAL_SIZE;
		if(i < ACC_VAL_SIZE){
			acc_val[m] = a2[i] * a1[i];
		} else {
			acc_val[m] += a2[i] * a1[i];
		}
	}
	/* Tree reduction to avoid critical path issues */
	my_double tmp_v_0 = acc_val[0] + acc_val[1];
	my_double tmp_v_1 = acc_val[2] + acc_val[3];
	my_double tmp_v_2 = acc_val[4] + acc_val[5];
	my_double tmp_v_3 = acc_val[6] + acc_val[7];
	my_double tmp_v_4 = acc_val[8] + acc_val[9];
	my_double tmp_v_5 = acc_val[10] + acc_val[11];
	my_double tmp_v_6 = acc_val[12] + acc_val[13];
	my_double tmp_v_7 = acc_val[14] + acc_val[15];

	my_double tmp_v_8 = tmp_v_0 + tmp_v_1;
	my_double tmp_v_9 = tmp_v_2 + tmp_v_3;
	my_double tmp_v_10 = tmp_v_4 + tmp_v_5;
	my_double tmp_v_11 = tmp_v_6 + tmp_v_7;

	my_double tmp_v_12 = tmp_v_8 + tmp_v_9;
	my_double tmp_v_13 = tmp_v_10 + tmp_v_11;

	my_double tmp_v_14 = tmp_v_12 + tmp_v_13;
	return tmp_v_14;
}

void division_by_NCOMP(my_double whiteM_div[NCOMP][COUNT],my_double whiteM[NCOMP][COUNT]){
	FOR_2:
	for (int x=0;x<NCOMP; x++)
		FOR_2_INTERNAL:
		for (int y=0;y<COUNT; y++)
		#pragma HLS PIPELINE
			whiteM_div[x][y]=whiteM[x][y]*DIV_NCOMP;

}

void W_updating(my_double W[NCOMP][NCOMP], my_double W_vett[NCOMP], my_int i){
	W_UPDATING:
	for (int x=0;x<NCOMP; x++)
		#pragma HLS PIPELINE
        W[x][i]=W_vett[x];
}


my_double addition(my_double f2_out[]){
#pragma HLS INLINE
	my_double W_p2=0;

	my_double acc_val[ACC_VAL_SIZE];

	for(int i=0;i<COUNT;i++){
#pragma HLS PIPELINE
/* Partials sum to avoid interdependency between for iterations, Delta of ACC_VALL_SIZE */
		int m = i % ACC_VAL_SIZE;
		if(i < ACC_VAL_SIZE){
			acc_val[m] = f2_out[i];
		} else {
			acc_val[m] += f2_out[i];
		}
	}
	/* Tree reduction to avoid critical path issues */
	my_double tmp_v_0 = acc_val[0] + acc_val[1];
	my_double tmp_v_1 = acc_val[2] + acc_val[3];
	my_double tmp_v_2 = acc_val[4] + acc_val[5];
	my_double tmp_v_3 = acc_val[6] + acc_val[7];
	my_double tmp_v_4 = acc_val[8] + acc_val[9];
	my_double tmp_v_5 = acc_val[10] + acc_val[11];
	my_double tmp_v_6 = acc_val[12] + acc_val[13];
	my_double tmp_v_7 = acc_val[14] + acc_val[15];

	my_double tmp_v_8 = tmp_v_0 + tmp_v_1;
	my_double tmp_v_9 = tmp_v_2 + tmp_v_3;
	my_double tmp_v_10 = tmp_v_4 + tmp_v_5;
	my_double tmp_v_11 = tmp_v_6 + tmp_v_7;

	my_double tmp_v_12 = tmp_v_8 + tmp_v_9;
	my_double tmp_v_13 = tmp_v_10 + tmp_v_11;

	my_double tmp_v_14 = tmp_v_12 + tmp_v_13;
	return tmp_v_14;
}
