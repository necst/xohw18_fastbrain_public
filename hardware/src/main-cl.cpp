/*
FastBRaIn is an implementation of FastICA to identify Resting State Networks, analyzing fMRI images.
FastBRaIn has been developed at NECSTLab, Politecnico di Milano.

Filippo Carloni:    filippo.carloni@mail.polimi.it
Giada Casagrande:   giada.casagrande@mail.polimi.it
Valentina Corbetta: valentina.corbetta@mail.polimi.it

*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include "support.hpp"

#include <complex.h>
#include <float.h>
#include <iostream>
#include <lapacke/lapacke.h>
#include <malloc.h>
#include "nifti1.h"
#include <stdint.h>
#include <string>
#include <time.h>
#include <vector>
#include <sstream>

using namespace std;

#define HEIGHT 512
#define MIN_HEADER_SIZE 348
#define NII_HEADER_SIZE 352
#define WEIGHT 1024


#define NCOMP 84    //NCOMP: Setting number of statistically significant components

//initialization of parameters needed in FastICA
#define NMAX 150  //maximum number of iterations
#define TOLL 0.0001 //tolerance
#define COUNT 42464 


typedef int my_int;
typedef float  my_double;
typedef char my_char;
typedef uint16_t MY_DATATYPE;



void acquire_brain(vector<MY_DATATYPE> &brain, MY_DATATYPE **data, int d1, int d2, int d3, int dim);
void get_diag (double **mat_diag, double *mat, int r, int c);
void get_eigvec (double **eigvec, double **p_eigvec, int r, int c);
void get_linear_img (int dim, int* indfinal, double **linear_img, int d1, int d2, int d3, MY_DATATYPE **data, int count);
int get_mask(double *xtemp, MY_DATATYPE **data,int dim, int d1, int d2, int d3, int *indfinal);
void get_matrix_mult(double **a1, double **a2, int r1, int c1, int c2, double **a_mult);
void get_matrix_mult_whiteM(double **a1, double **a2, int r1, int c1, int c2, my_double **a_mult);
void get_mean (double **mat, int r_mat, int c_mat, double **vett_col);
void get_sqrt (double **a, double **a_sqrt, int r, int c );
void get_transp(double **a, double **aT, int r_a, int c_a);
void get_transp_W(my_double **a, double **aT, int r_a, int c_a);
void limit_values(vector<vector<double> > &slice, int d_x, int d_y, int indfinal[], int ind_in);
int load_nii (char *hdr_file, int n, int dim, MY_DATATYPE **data, int *dim1, int *dim2, int *dim3);
void ones_mat(double **one_mat, int r, int c);
void save_bmp_image(unsigned char img[HEIGHT][WEIGHT][3], int nc, char *directories);
void set_range_after_smoothing(vector<vector<double> > &slice, int d_x, int d_y, int indfinal[], int ind_in);
void show_comp(int d_x, int d_y, int d_z, double **source, int indfinal[], vector<MY_DATATYPE> &brain, char *path);
void show_slice(unsigned char img[HEIGHT][WEIGHT][3], int x_start, int y_start, vector<vector<double> > &slice, int d_x, int d_y, int z, vector<MY_DATATYPE> &brain);
void smoothing_10(int d_x, int d_y, vector<vector<double> > &slice);
void vett_from_mat(double **vett, int r_vett, double **mat, int ind);
void zeros_mat(double **mat, int r, int c);




/*
 * Given an event, this function returns the kernel execution time in ms
 */
float  getTimeDifference(cl_event event)
{
	cl_ulong	time_start = 0;
	cl_ulong	time_end = 0;
	float		total_time = 0.0;

	clGetEventProfilingInfo(event,
				CL_PROFILING_COMMAND_START,
				sizeof(time_start),
				&time_start,
				NULL);
	clGetEventProfilingInfo(event,
				CL_PROFILING_COMMAND_END,
				sizeof(time_end),
				&time_end,
				NULL);
	total_time = time_end - time_start;
	return total_time / 1000000.0;
	//To convert nanoseconds to milliseconds
}

/*zeros_mat: function to initialize the elements of the input matrix to zero*/
void zeros_mat_f(float **mat, int r, int c) {
    int i,j;

    for (i=0;i<r;i++)
        for(j=0;j<c;j++)
            mat[i][j]=0.0;
}

int load_file_to_memory(const char *filename, char **result)
{
    size_t size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL)
        {
            *result = NULL;
            return -1; // -1 means file opening fail
        }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *)malloc(size+1);
    if (size != fread(*result, sizeof(char), size, f))
        {
            free(*result);
            return -2; // -2 means file reading fail
        }
    fclose(f);
    (*result)[size] = 0;
    return size;
}

void traspose_white(my_double whiteM[][COUNT],my_double white_m_t[][NCOMP]){
	for(int i=0;i<NCOMP;i++)
		for(int j=0;j<COUNT;j++)
			white_m_t[j][i]=whiteM[i][j];

}

void run_FPGA( my_double *wmat, my_double *whitemat, int *failure_out, cl_context context, cl_command_queue commands, cl_program program, cl_kernel kernel){
	my_double *whitematt = (my_double *)malloc(sizeof(my_double) * NCOMP*COUNT);
	traspose_white((my_double(*)[COUNT])whitemat,(my_double(*)[NCOMP])whitematt);
	cl_mem     w_buff, white_m_buff, white_m_t_buff, failure_buff;


    white_m_buff = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(my_double) * NCOMP*COUNT, NULL, NULL);
    white_m_t_buff = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(my_double) * NCOMP*COUNT, NULL, NULL);
	failure_buff = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(my_int), NULL, NULL);
    w_buff = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(my_double)* NCOMP* NCOMP, NULL, NULL);

		int		error;
		
		//write buffers for the	kernel

		error = clEnqueueWriteBuffer(commands, white_m_buff, CL_TRUE, 0,sizeof(my_double) * NCOMP*COUNT, whitemat, 0, NULL, NULL);
		if (error != CL_SUCCESS) {
			printf("error while writing the buffer!! whitemat\n");
			exit(EXIT_FAILURE);
		}
		error = clEnqueueWriteBuffer(commands, white_m_t_buff, CL_TRUE, 0,sizeof(my_double) * NCOMP*COUNT, whitematt, 0, NULL, NULL);
		if (error != CL_SUCCESS) {
			printf("error while writing the buffer!! whitematt\n");
			exit(EXIT_FAILURE);
		}
		error = clEnqueueWriteBuffer(commands, w_buff, CL_TRUE, 0,sizeof(my_double) * NCOMP*NCOMP, wmat, 0, NULL, NULL);
		if (error != CL_SUCCESS) {
			printf("error while writing the buffer!! wmat\n");
			exit(EXIT_FAILURE);
		}



		//set the arguments for the kernel
		error = 0;


		error = clSetKernelArg(kernel, 0, sizeof(cl_mem), &w_buff);
		if (error != CL_SUCCESS) {
			printf("Error: Failed to set kernel arguments 0! %d\n", error);
			printf("Test failed\n");
			exit(EXIT_FAILURE);
		}
		error = clSetKernelArg(kernel, 1, sizeof(cl_mem), &white_m_buff);
		if (error != CL_SUCCESS) {
			printf("Error: Failed to set kernel arguments 0! %d\n", error);
			printf("Test failed\n");
			exit(EXIT_FAILURE);
		}
		error = clSetKernelArg(kernel, 2, sizeof(cl_mem), &white_m_t_buff);
		if (error != CL_SUCCESS) {
			printf("Error: Failed to set kernel arguments 0! %d\n", error);
			printf("Test failed\n");
			exit(EXIT_FAILURE);
		}
		error = clSetKernelArg(kernel, 3, sizeof(cl_mem), &failure_buff);
		if (error != CL_SUCCESS) {
			printf("Error: Failed to set kernel arguments 0! %d\n", error);
			printf("Test failed\n");
			exit(EXIT_FAILURE);
		}
		//Execute the kernel over the entire range of our 1 d input data set
		// using the maximum number of work group items for this device

		error = 1;
		cl_event enqueue_kernel;

		double start_time, end_time;

		start_time = get_time();

#ifdef C_KERNEL
		error = clEnqueueTask(commands, kernel, 0, NULL, &enqueue_kernel);
#endif
		if (error){
			printf("Error: Failed to execute kernel! %d\n", error);
			printf("Test failed\n");
			exit(EXIT_FAILURE);
		}


		clWaitForEvents(1, &enqueue_kernel);

		end_time = get_time();

		float executionTime = getTimeDifference(enqueue_kernel);
		printf(" execution time is %f ms \n", executionTime);


		printf("Global execution time is %f s \n", end_time - start_time);

		//Read back the results from the device to verify the output
		cl_event readEvent_0, readEvent_1;
		error = clEnqueueReadBuffer(commands,  w_buff, CL_TRUE, 0, sizeof(my_double) *NCOMP*NCOMP, wmat, 0, NULL, &readEvent_0);
		error |= clEnqueueReadBuffer(commands,  failure_buff, CL_TRUE, 0, sizeof(int), failure_out, 0, NULL, &readEvent_1);

		if (error != CL_SUCCESS) {
			printf("error in reading the output!! %d \n", error);
			fflush(stdout);
			//return EXIT_FAILURE;
		}
		clWaitForEvents(1, &readEvent_0);
		clWaitForEvents(1, &readEvent_1);


    free(whitematt);
}

void cpwhitemat(my_double whitemat[][COUNT],my_double **W){
	for(int i=0;i<NCOMP; i++)
		for(int x=0;x < COUNT;x++)
			whitemat[i][x]=W[i][x];

}

void cpwmat(my_double whitemat[][NCOMP],my_double **W){
	for(int i=0;i<NCOMP; i++)
		for(int x=0;x < NCOMP;x++)
			whitemat[i][x]=W[i][x];
}

void cpoutput(my_double whitemat[][NCOMP],my_double **W){
	for(int i=0;i<NCOMP; i++)
		for(int x=0;x < NCOMP;x++)
			W[i][x]=whitemat[i][x];
}

int
main(int argc, char **argv)
{
	// argv[2]  path to the file containing the name of the images (in this case nomi.txt)
	// argv[3]  path to the input directory for images
	// argv[4]  path to save the output images
#if defined(SDX_PLATFORM) && !defined(TARGET_DEVICE)
  #define STR_VALUE(arg)      #arg
  #define GET_STRING(name) STR_VALUE(name)
  #define TARGET_DEVICE GET_STRING(SDX_PLATFORM)
#endif

	if(argc < 5){
		printf("Please, provide the following arguments:\n- the xclbin file\n- the name.txt\n the input directories path for the images\n output directories path\n");
		return 1;
	}

	printf("starting HOST code \n");
	fflush(stdout);
	int		err;

    cl_platform_id platform_id;         // platform id
    cl_device_id device_id;             // compute device id
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute program
    cl_kernel kernel;                   // compute kernel

    char cl_platform_vendor[1001];
	char cl_platform_name[1001];
    // Connect to first platform
	  //
		printf("GET platform \n");
	  err = clGetPlatformIDs(1,&platform_id,NULL);
	  if (err != CL_SUCCESS)
	  {
	    printf("Error: Failed to find an OpenCL platform!\n");
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	  }
		printf("GET platform vendor \n");
	  err = clGetPlatformInfo(platform_id,CL_PLATFORM_VENDOR,1000,(void *)cl_platform_vendor,NULL);
	  if (err != CL_SUCCESS)
	  {
	    printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	  }
	  printf("CL_PLATFORM_VENDOR %s\n",cl_platform_vendor);
		printf("GET platform name \n");
	  err = clGetPlatformInfo(platform_id,CL_PLATFORM_NAME,1000,(void *)cl_platform_name,NULL);
	  if (err != CL_SUCCESS)
	  {
	    printf("Error: clGetPlatformInfo(CL_PLATFORM_NAME) failed!\n");
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	  }
	  printf("CL_PLATFORM_NAME %s\n",cl_platform_name);

	  // Connect to a compute device
	  //
	  int fpga = 0;
	//#if defined (FPGA_DEVICE)
	  fpga = 1;
	//#endif
		printf("get device \n");
	  err = clGetDeviceIDs(platform_id, fpga ? CL_DEVICE_TYPE_ACCELERATOR : CL_DEVICE_TYPE_CPU,
	                       1, &device_id, NULL);
	  if (err != CL_SUCCESS)
	  {
	    printf("Error: Failed to create a device group!\n");
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	  }

	  // Create a compute context
	  //
		printf("create context \n");
	  context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	  if (!context)
	  {
	    printf("Error: Failed to create a compute context!\n");
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	  }

	  // Create a command commands
	  //
		printf("create queue \n");
	  commands = clCreateCommandQueue(context, device_id, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE, &err);
	  if (!commands)
	  {
	    printf("Error: Failed to create a command commands!\n");
	    printf("Error: code %i\n",err);
	    printf("Test failed\n");
	    return EXIT_FAILURE;
	  }


	  int status;

	// Create Program Objects
	  //

	  // Load binary from disk
	  unsigned char *kernelbinary;
	  char *xclbin = (char *)malloc(sizeof(char) * (strlen(argv[1]) + 1));
	  strcpy(xclbin, argv[1]);
	  printf("loading %s\n", xclbin);
	  int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
	  if (n_i < 0) {
		printf("failed to load kernel from xclbin: %s\n", xclbin);
		printf("Test failed\n");
		return EXIT_FAILURE;
	  }
	  size_t n_kernel = n_i;
	  // Create the compute program from offline
		printf("create program with binary \n");
	  program = clCreateProgramWithBinary(context, 1, &device_id, &n_kernel,
										  (const unsigned char **) &kernelbinary, &status, &err);
	  if ((!program) || (err!=CL_SUCCESS)) {
		printf("Error: Failed to create compute program from binary %d!\n", err);
		printf("Test failed\n");
		return EXIT_FAILURE;
	  }
	  printf("p");

	  // Build the program executable
	  //
		printf("build program \n");
	  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	  if (err != CL_SUCCESS)
	  {
		size_t len;
		char buffer[2048];

		printf("Error: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
		printf("Test failed\n");
		return EXIT_FAILURE;
	  }

	  // Create the compute kernel in the program we wish to run
	  //
	  printf("create kernel \n");

	  kernel = clCreateKernel(program, "kernel", &err);
	  if (!kernel || err != CL_SUCCESS)
	  {
		printf("Error: Failed to create compute kernel!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	  }



	int  dim=250; //dim represents the number of fMRI temporal scans (3D images in NIfTI file format)
    int i,j,x,y,k, n=0, d1, d2, d3, nmax, count, cont, len, val, index_vet;
    int *dim1=NULL, *dim2=NULL, *dim3=NULL, *indfinal=NULL;
    char *str1, *res;
    double temperr,tol, sdev, **mt, a, b, norm, *xtemp=NULL, **linear_img, **linear_imgT, **meanL, **covMatrix, **f_in, **f1_out, **f1_out_T, **W_parte1, **W_parte2,  **whiteM_div;
    double **whiteningMatrix,  **eigval_sqrt,  **eigval_sqrt_inv, **f2_out, **one_mat,**one_matT ,**source1, **source2, **source, **sourceT, **meanS, **meanST;
    double **source_vett, **source_vettT, *W_p, **V, **eigval, **U_p, **W_diag, **eigvec, **eigvecT, **WT,**W_mult, **wmem,**wmemT, **wtemp, **W_vett, **W_vett_j, **W_vettT, **W_p2, **skew;
    my_double **whiteM, **W;
    FILE *fd;
    MY_DATATYPE **data=NULL;

    /*============ Allocating memory ===========*/

    //Allocating the rows of the matrix of data: allocation of a vertical array made of 'dim' cells of pointers
    data = (MY_DATATYPE **) malloc(sizeof(MY_DATATYPE *) * dim);

    dim1 = (int*) malloc(sizeof(int)*1); /*dimension x of 3D image*/
    dim2 = (int*) malloc(sizeof(int)*1); /*dimension y of 3D image*/
    dim3 = (int*) malloc(sizeof(int)*1); /*dimension z of 3D image*/


    /*========== Reading .nii files ==========*/

    /*The fMRI images analyzed are in the NIfTI format, which is an Analyze-style data format,
     proposed by the NIfTI DFWG as a short-term measure to facilitate inter-operation
     of functional MRI data analysis software packages.*/

    /*In C and C++, the access to a file can be made using a pointer to FILE,
     which is the file containing the information about the fMRI images*/

    // in argv[2] file names of .nii files are saved:in this case there are 250 strings of 43 characters each
    fd=fopen(argv[2],"r");

    if (fd==NULL)
    //if the file does not exist the output is NULL
        printf("No file with images name opened.\n");

    for (i=0; i<dim; i++) {
        //length of the string (name of the files in argv[2])
        str1 = (char*) malloc(sizeof(char)*44);

        /*fgets(char* str,int num,FILE* stream): function to read characters from stream and stores
        them as a C string into str until (num-1) characters have been read or either a newline or
        the end-of-file is reached.
        A newline character makes fgets stop reading, but it is considered a valid character by the
        function and included in the string copied to str.
        A terminating null character is automatically appended after the characters copied to str.*/
        res=fgets(str1,44, fd);
        if(res==NULL){
            return 0;
        }


        /*========== Loading the 3D images ==========*/
        printf("Loading volume %d.\n",n);
        //Each 3D image is composed of dim1 x dim2 x dim3 pixels
        char path[200];
        strcpy(path,argv[3]);
        str1=strcat(path, str1);
        n=load_nii(str1, n, dim, data, dim1, dim2, dim3);
        d1=*dim1;
        d2=*dim2;
        d3=*dim3;
    }
    fclose(fd);
    printf("End of images loading.\n");


    /*========== Starting preprocessing ==========*/

    /*=== masking 3d image ===*/
    /*It is necessary to identify a Region of Interest (ROI) and the background;
     to do so, a masking process is performed. First, the mean is calculated and only the
     elements with a value superior to the mean are kept. Second, the mean is subtracted from the remaining values,
     to centre the data*/

    len= d1*d2*d3;
    xtemp = (double *) malloc(sizeof(double)* len);
    indfinal = (int *) malloc(sizeof(int)* len);
    count=get_mask(xtemp, data, dim, d1, d2, d3, indfinal);
    //The image is linearized to obtain a (dim x count) matrix
    linear_img = (double **) malloc(sizeof(double *) * dim);
    for (i=0;i<dim; i++)
        linear_img[i]=(double*)malloc(sizeof(double)* count);

    get_linear_img(dim, indfinal, linear_img,d1,d2,d3, data, count);

    //removing the mean: meanL is a column vector containing the mean at each temporal instant
    meanL =  (double **) malloc(sizeof(double*)* dim);
    for (i=0;i<dim; i++)
        meanL[i]=(double*)malloc(sizeof(double)*1);

    //calulating the mean at each temporal instant and subtracting it from the row of the corresponding time
    get_mean (linear_img, dim , count, meanL);

    for(i=0;i<dim; i++)
        for (j=0;j<count; j++)
            linear_img[i][j]=linear_img[i][j] - meanL[i][0];
			
    /*========== dimension reduction by PCA and whitening ==========*/

    /*Part of the preprocessing consists in data reduction, by performing the Principal Component Analysis (PCA),
     which reduces the number of random variables considered by obtaining a set of principal variables. This is
     done to eliminate the variables which do not contain relevant information.
     PCA can be used to perform the whitening of the data: after centering the data, the decomposition in eigenvectors
     and eigenvalues of the matrix of the covariance is performed.*/

    covMatrix= (double **) malloc(sizeof(double *)*dim);
    for (i=0; i<dim; i++)
        covMatrix[i] = (double *) malloc(sizeof(double )*dim);

    linear_imgT = (double **) malloc(sizeof(double *) * count);
    for (i=0;i<count; i++)
        linear_imgT[i]=(double*)malloc(sizeof(double)* dim);

    //The transpose of the linear image is needed, after the removal of the mean
    get_transp (linear_img, linear_imgT, dim, count);

    //Function necessary to obtain the matrix of the covariance of linear_img
    get_matrix_mult(linear_img, linear_imgT, dim, count, dim, covMatrix);

    //Dividing every element of the matrix of the covariance for count
    for (i=0; i<dim; i++)
        for (j=0; j<dim; j++)
            covMatrix[i][j]=covMatrix[i][j] / (count);


    //Allocating memory for the matrices of eigenvalues and eigenvectors
    W_p=(double*) malloc(sizeof(double)*dim);

    U_p= (double **) malloc(sizeof(double *)*dim);
    for (i=0; i<dim; i++)
        U_p[i] = (double *) malloc(sizeof(double )*dim);

    eigvec= (double **) malloc(sizeof(double *)*dim);
    for (i=0; i<dim; i++)
        eigvec[i] = (double *) malloc(sizeof(double )*NCOMP);

    eigvecT= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigvecT[i] = (double *) malloc(sizeof(double )*dim);

    W_diag = (double **) malloc(sizeof(double *)*dim);
    for (i=0; i<dim; i++)
        W_diag[i] = (double *) malloc(sizeof(double )*dim);

    eigval = (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigval[i] = (double *) malloc(sizeof(double )*NCOMP);
    
	//Using LAPACKE (an interface to use LAPACK from C) libraries and functions to calculate eigenvalues and eigenvectors
    lapack_int nl = dim, lda= dim, info;
    double cov_lapack[nl*nl], eigvalues[nl];

    //Coping elements of covMatrix in cov_lapack
    for (i=0;i<dim;i++)
    	for(j=0;j<dim;j++)
    		cov_lapack[dim*i+j]=covMatrix[i][j];

    /*LAPACKE_dsyev computes all eigenvalues and eigenvectors of an nl-by-nl real symmetric matrix
        input: 1) LAPACK_ROW_MAJOR: interpretation of memory as row-major
               2) 'V':  computes eigenvalues and eigenvectors
               3) 'U':  upper triangle of the real symmetric matrix is stored
               4) nl: the order of the matrix cov_lapack (nl>=0)
               5) cov_lapack: real symmetric matrix (it is overwritten with eigenvectors matrix)
               6) lda: the leading dimension of the array ( lda>=max(1,nl))
               7) eigvalues: vector of eigenvalues in ascending order
        output: info
                check for convergence:
                     = 0:  successful exit
                     < 0:  if info=-i, the i-th argument had an illegal value
                     > 0:  if info=i, the algorithm failed to converge; i off-diagonal elements of an intermediate tridiagonal
                          form did not converge to zero
        */
    info=LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'V', 'U', nl, cov_lapack, lda, eigvalues);
    if (info!=0) {
    	printf("The algorithm failed");
    	return 0;
    }

    //Coping the vector of eigenvalues in W_p, sorting them in descending order
    for(i=0;i<nl;i++)
        W_p[nl-1-i]=eigvalues[i];

    //Diagonalization of W_p to obtain W_diag
    get_diag (W_diag, W_p, dim, dim);
    //Reduction of W_diag to eigval, a diagonal matrix containing the NCOMP most significant eigenvalues
    for (i=0;i<NCOMP; i++)
        for (j=0;j<NCOMP; j++)
            eigval[i][j]=W_diag[i][j];

    //Coping the eigenvectors obtained using LAPACKE_dsyev in U_p, sorting also columns
    for (i=0;i<nl;i++)
        for(j=0;j<nl;j++)
            U_p[i][nl-1-j]=cov_lapack[i*nl+j];

    //Getting eigvec matrix of eigenvectors corresponding to the NCOMP most significant eigenvalues saved
    get_eigvec (eigvec, U_p, dim, NCOMP);


    whiteningMatrix= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        whiteningMatrix[i] = (double *) malloc(sizeof(double )*dim);


    whiteM= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        whiteM[i] = (my_double *) malloc(sizeof(my_double )*count);


    whiteM_div= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        whiteM_div[i] = (double *) malloc(sizeof(double )*count);

    eigval_sqrt= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigval_sqrt[i] = (double *) malloc(sizeof(double )*NCOMP);

    eigval_sqrt_inv= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigval_sqrt_inv[i] = (double *) malloc(sizeof(double )*NCOMP);


    get_transp(eigvec, eigvecT, dim, NCOMP);
    get_sqrt(eigval, eigval_sqrt, NCOMP, NCOMP);


    for (i=0;i<NCOMP; i++)
        for (j=0;j<NCOMP; j++){
            if(eigval_sqrt[i][j])
                eigval_sqrt_inv[i][j]= 1.0/eigval_sqrt[i][j];
            else
                eigval_sqrt_inv[i][j]= eigval_sqrt[i][j];
        }


    //Obtaining the whitened matrix
    get_matrix_mult(eigval_sqrt_inv, eigvecT, NCOMP, NCOMP, dim, whiteningMatrix);
    get_matrix_mult_whiteM(whiteningMatrix, linear_img, NCOMP, dim , count , whiteM);


   /* ========== End of preprocessing ==========*/

   
	// Allocating memory for FastICA computation

    W= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    WT= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        WT[i] = (double *) malloc(sizeof(double )*NCOMP);

    wmem= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        wmem[i] = (double *) malloc(sizeof(double )*1);

    wmemT= (double **) malloc(sizeof(double *)*1);
    for (i=0; i<1; i++)
        wmemT[i] = (double *) malloc(sizeof(double )*NCOMP);

    wtemp= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        wtemp[i] = (double *) malloc(sizeof(double )*1);

    W_vett= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_vett[i] = (double *) malloc(sizeof(double )*1);

    W_vett_j= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_vett_j[i] = (double *) malloc(sizeof(double )*1);

    W_vettT= (double **) malloc(sizeof(double *)*1);
    for (i=0; i<1; i++)
        W_vettT[i] = (double *) malloc(sizeof(double )*NCOMP);

    W_mult= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_mult[i] = (double *) malloc(sizeof(double )*dim);

    f_in= (double **) malloc(sizeof(double *)*1);
    for (i=0; i<1; i++)
        f_in[i] = (double *) malloc(sizeof(double )*count);

    f1_out= (double **) malloc(sizeof(double *)*1);
    for (i=0; i<1; i++)
        f1_out[i] = (double *) malloc(sizeof(double )*count);

    f1_out_T= (double **) malloc(sizeof(double *)*count);
    for (i=0; i<count; i++)
        f1_out_T[i] = (double *) malloc(sizeof(double )*1);

    W_parte1= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_parte1[i] = (double *) malloc(sizeof(double )*1);

    W_parte2= (double **) malloc(sizeof(double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_parte2[i] = (double *) malloc(sizeof(double )*1);

    f2_out = (double**) malloc(sizeof(double*)*1);
    for (x=0;x<1;x++)
        f2_out [x]=(double*)malloc(sizeof(double)*count);

    one_mat = (double**) malloc(sizeof(double*)*count);
    for (x=0;x<count;x++)
            one_mat [x]=(double*)malloc(sizeof(double)*1);

    one_matT = (double**) malloc(sizeof(double*)*1);
    for (x=0;x<1;x++)
            one_matT [x]=(double*)malloc(sizeof(double)*count);

    W_p2= (double**) malloc(sizeof(double*)*1);
    for (x=0;x<1;x++)
            W_p2[x]=(double*)malloc(sizeof(double)*1);

    source1= (double**) malloc(sizeof(double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            source1[x]=(double*)malloc(sizeof(double)*count);

    source2= (double**) malloc(sizeof(double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            source2[x]=(double*)malloc(sizeof(double)*count);

    source= (double**) malloc(sizeof(double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            source[x]=(double*)malloc(sizeof(double)*count);

    sourceT= (double**) malloc(sizeof(double*)*count);
    for (x=0;x<count;x++)
            sourceT[x]=(double*)malloc(sizeof(double)*NCOMP);

    meanS= (double**) malloc(sizeof(double*)*1);
    for (x=0;x<1;x++)
            meanS[x]=(double*)malloc(sizeof(double)*NCOMP);

    meanST= (double**) malloc(sizeof(double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            meanST[x]=(double*)malloc(sizeof(double)*1);

    skew= (double**) malloc(sizeof(double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            skew[x]=(double*)malloc(sizeof(double)*1);

    //A matrix full of random numbers is needed to perform FastICA
    srand(time(NULL));
    a = 0.5;
    for (i=0;i<NCOMP;i++)
        for (j=0;j<NCOMP;j++)
            //Limiting rand function to obtain only float numbers between 0 and 1
            W[i][j]=(float)(rand()/(float)(RAND_MAX)) * a;


   //Normalization of the column vectors of W
    for (i=0;i<NCOMP; i++) {
        norm=0.0;
        for (x=0;x<NCOMP;x++)
            norm=norm + W[x][i]*W[x][i];
        norm=sqrt(norm);
        for (x=0;x<NCOMP; x++)
            W[x][i]=W[x][i]/norm;
    }



	/* run FPGA */
	int kernel_status;
	printf("Running on FPGA...\n");
	my_double *wmat =(my_double *)malloc(sizeof(my_double) * NCOMP*NCOMP);
	my_double *whitemat = (my_double *)malloc(sizeof(my_double) * NCOMP*COUNT);
	cpwhitemat((my_double (*)[COUNT])whitemat, whiteM);
	cpwmat((my_double (*)[NCOMP])wmat, W);
	run_FPGA(wmat,whitemat,&kernel_status,  context, commands, program, kernel);
	cpoutput((my_double (*)[NCOMP])wmat,W);
	
    printf("Done!\n");
    clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

	//Adding mean back to the data
    get_transp_W(W, WT, NCOMP, NCOMP);
    get_matrix_mult( WT, whiteningMatrix, NCOMP, NCOMP, dim, W_mult);
    get_matrix_mult( W_mult, linear_img, NCOMP, dim, count, source1);
    get_transp(one_mat, one_matT, count, 1);
    get_matrix_mult( W_mult, meanL, NCOMP, dim, 1, meanST);
    get_matrix_mult( meanST, one_matT, NCOMP , 1, count, source2);

    //Obtaining the source matrix after FastICA
    for (x=0;x<NCOMP; x++)
        for (y=0;y<count;y++)
            source[x][y]=source1[x][y]+source2[x][y];


    /*========== Choosing sign by skewness ==========*/

    /*Skewness is a measure of lack of simmetry in a statistical distribution; it helps to
    understand how much a distribution differs from a normal one*/

    //Obtaining the mean of source: meanST
    get_mean (source, NCOMP , count, meanST);
    get_transp(meanST, meanS, NCOMP, 1);
    zeros_mat(skew, NCOMP, 1);
    for (i=0;i<NCOMP; i++) {
        a=0.0;
        for (j=0; j<count; j++) {
            a=a+(double)pow((source[i][j] - meanS[0][i]),2.0);
            skew[i][0]=skew[i][0] + (double)pow((source[i][j] - meanS[0][i]),3.0);
        }
        b=sqrt(a/(double)count);
        b=(double)pow(b,3.0);
        skew[i][0]=(skew[i][0]/(double)count)/b;
    }

    for (i=0;i<NCOMP; i++)
        if (skew[i][0] <0)
            for(j=0;j<count;j++)
                source[i][j] = -source[i][j];


    /*========== Conversion to z-score ==========*/

    //Allocating memory
    source_vett=(double**)malloc(sizeof(double*)*1);
    for (i=0;i<1;i++)
        source_vett[i]=(double*)malloc(sizeof(double)*count);

    source_vettT=(double**)malloc(sizeof(double*)*count);
    for (i=0;i<count;i++)
        source_vettT[i]=(double*)malloc(sizeof(double)*1);

    mt=(double**)malloc(sizeof(double*)*1);
    for (i=0;i<1;i++)
        mt[i]=(double*)malloc(sizeof(double)*1);

    get_transp(source, sourceT,NCOMP, count);

    /*Calculating the standard deviation to quantify the amount of variation
    or dispersion of our set of data*/
    for (i=0;i<NCOMP; i++) {
        vett_from_mat(source_vettT, count, sourceT, i);
        get_transp(source_vettT, source_vett, count, 1);

        //Obtainig the standard deviation of each column
        get_mean(source_vett, 1 , count, mt);
        sdev=0.0;
        for (x=0;x<count; x++)
            sdev= sdev + pow(((source_vett[0][x]) - mt[0][0]), 2.0);
        sdev=sqrt(sdev/(double)count);
        for (x=0;x<count; x++)
            source[i][x]=((source_vett[0][x])-mt[0][0])/ ((double)sdev);
    }



    /*========== Saving components ==========*/

    printf("\nStart saving pictures...\n");

    // Extracting the background of the brain images to show the components
    vector<MY_DATATYPE> brain(len);
    acquire_brain(brain, data, d1, d2, d3, dim);
    // Saving images of all the components
    show_comp(d1, d2, d3, source, indfinal, brain,argv[4]);

    printf("\n\nPictures saved\n");
    printf("End: final components obtained\n");



	return EXIT_SUCCESS;
}



/*acquire_brain: function to get brain images for reconstruction of final fastICA components*/
void acquire_brain(vector<MY_DATATYPE> &brain, MY_DATATYPE **data, int d1, int d2, int d3, int dim){
    int len = d1*d2*d3;
    int i, j, k;
    double max, min, range;

    for(i=0;i<len;i++)
    {
        brain[i] = data[dim/2][i];
    }

    for(k=0;k<d3;k++)
    {
        min = 1000000;
        max = -1000000;
        for(j=0;j<d2*d1;j++)
        {
            if(brain[k*d2*d1 + j] < min) min = brain[k*d2*d1 + j];
            if(brain[k*d2*d1 + j] > max) max = brain[k*d2*d1 + j];
        }
        range = max - min;
        for(j=0;j<d2*d1;j++)
        {
            brain[k*d2*d1 + j] = round(((brain[k*d2*d1 + j] - min)/range)*255);
        }
    }
}

/*get_diag: function to obtain a matrix, which has the elements of the input vector
 on the diagonal and zeros in each other position.*/
void get_diag (double **mat_diag, double *mat, int r, int c) {
    int i, j;
    for (i=0;i<r; i++)
        for (j=0;j<c; j++){
            if (i==j)
                mat_diag[i][j]=mat[i];
            else
                mat_diag[i][j]=0;
        }
}


/*get_eigvec: function to save only the first c columns of the whole matrix of eigenvectors */
void get_eigvec (double **eigvec, double **p_eigvec, int r, int c){
    int i, j;
    for (i=0;i<r; i++)
        for (j=0;j<c; j++)
            eigvec[i][j]=p_eigvec[i][j];
}


/*get_linear_img: function to obtain masked linearized data*/
void get_linear_img (int dim, int* indfinal, double **linear_img, int d1, int d2, int d3, MY_DATATYPE **data,int count) {
    int i, j, k=0;

    for (i=0;i<dim; i++){
        k=0;
        for (j=0;j<d1*d2*d3;j++){
            if (indfinal[j]){
                linear_img[i][k]=data[i][j];
                k++;
            }
        }
    }
}


/*get_mask: function to create the mask in pre-processing*/
int get_mask(double *xtemp, MY_DATATYPE **data, int dim, int d1, int d2, int d3,  int *indfinal){
    int i, j, count=0, o;
    double sum=0, m;

    for (i=0; i<d1*d2*d3; i++)
        xtemp[i]=data[(dim/2)-1][i];

    //Obtaining the mean of vector xtemp
    for (i=0;i<d1*d2*d3; i++)
        sum=sum+xtemp[i];
    m=sum/(double)(d1*d2*d3);


    for (i=0;i<d1*d2*d3; i++){
        if (xtemp[i]>=m){
            // in indfinal vector, 1 is set when the corresponding value in xtemp is greater than or equal to the mean
            indfinal[i]=1;
            //counting how many times the previous condition is verified
            count++;
        }
        else
            // in indfinal vector, 0 is set when the corresponding value in xtemp is less than the mean
            indfinal[i]=0;
    }

    //it returns how many 1 there are in indfinal, which represents the number of columns in linear_img
    return count;
}



/*get_matrix_mult: function to calculate the multiplication of two matrices.
 The output matrix has dimensions: r1 x c2.*/
void get_matrix_mult(double **a1, double **a2, int r1, int c1, int c2, double **a_mult) {
    int i, j, k;

    //Initializing the elements of the output matrix to zero
    zeros_mat(a_mult,r1,c2);

    for(i=0; i<r1; i++ ){
        for(j=0; j<c2; j++){
            for(k=0; k<c1; k++){
                a_mult[i][j] = a_mult[i][j] + a1[i][k] * a2[k][j];

            }
        }

    }

}


/*get_mean: function to calculate the mean of each row of a matrix.
 The output is a column vector, containing the mean of the rows.*/
void get_mean (double **mat, int r_mat, int c_mat, double **vett_col) {
    int i, j;
    double sum;

    for (i=0;i<r_mat; i++) {
        sum=0;
        for (j=0; j<c_mat; j++)
            sum=sum+mat[i][j];
        vett_col[i][0]=sum/(double)c_mat;

    }
 }

 /*get_sqrt: function to save the square root of the elements of the input matrix
 in the output matrix*/
void get_sqrt (double **a, double **a_sqrt, int r, int c ) {
    int i, j;
    for (i=0;i<r;i++)
        for (j=0;j<c;j++)
            a_sqrt[i][j]=sqrt(fabs(a[i][j]));
}

/*get_transp: function to obtain the transposed matrix of the input matrix*/
void get_transp(double **a, double **aT, int r_a, int c_a){
    int i, j;
    for (i=0;i<r_a;i++)
        for(j=0;j<c_a;j++)
            aT[j][i]=a[i][j];
}

void get_transp_W(my_double **a, double **aT, int r_a, int c_a){
    int i, j;
    for (i=0;i<r_a;i++)
        for(j=0;j<c_a;j++)
            aT[j][i]=a[i][j];
}


/*limit_values: function to set parameters for image reconstruction*/
void limit_values(vector<vector<double> > &slice, int d_x, int d_y, int indfinal[], int ind_in){
    int i, j;
    double max = -1000000;
    double lim_val = 3;

    for(i=0;i<d_y;i++)
        {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                if(slice[i][j] > lim_val)
                {
                    if(slice[i][j] > max) max = slice[i][j];
                }
                else
                {
                    slice[i][j] = lim_val;
                }
            }
        }
    }

    double range = max - lim_val;

    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                slice[i][j] = ((slice[i][j]-lim_val)/(range))*255;
            }
        }
    }
}


/*load_nii:function to save a volume in an array, linearized --> array: dim1 x dim2 x dim3
*hdr_file: is a pointer to the string where the name of the .nii file is stored;
n: is the number of the current volume*/
int load_nii (char *hdr_file,int n, int dim, MY_DATATYPE **data, int *dim1, int *dim2, int *dim3){

    nifti_1_header  hdr;
    FILE *fp;
    int ret,i,j,k;
    double total;

    /*========= read_nifti_file ==========*/
    //***open and read hdr_file***/

    // fopen: function to open the file saved in hdr_file, with a read mode
    fp= fopen(hdr_file, "r");
    if (fp==NULL)
        printf("No nii file opened.\n");

    /*fread: function to read the elements in a vector from a stream, in this case from fd.
    Input: 1)address in which data are saved
           2)dimension of each element in byte (MIN_HEADER_SIZE = 348)
           3)number of elements that must be read
           4)pointer to the file from which data are read
    Output: the output value must be equal to the third input, otherwise an error has occured*/
    ret= fread(&hdr, MIN_HEADER_SIZE, 1, fp);
    if (ret != 1)
        printf("No file has been read\n");

    //****** print header info ******/
    //  printf("%s header information\n", hdr_file);
    //  printf("array dimensions: %d %d %d %d \n", hdr.dim[1], hdr.dim[2], hdr.dim[3], hdr.dim[4]);
    //  printf("Datatype and bits/pixel: %d %d \n", hdr.datatype, hdr.bitpix);
    //  printf("Scaling slope and intercept: %.6f %.6f\n", hdr.scl_slope, hdr.scl_inter);
    //  printf("Byte offset to data in datafile: %ld \n", (long)(hdr.vox_offset));

    //A cast is needed to get an int, since hdr.dim is of type short
    *dim1=(int)hdr.dim[1];
    *dim2=(int)hdr.dim[2];
    *dim3=(int)hdr.dim[3];

    //****** read hdr_file and jump to data offset ******/
    /*fseek: function to set the index of the position of the file. The next operation of I/O on stream will
     be executed from the new position that has been set. The position is obtained adding offset,
     which can also be a negative value, to SEEK_SET.
     Input: 1)file from which the data are read
            2)how many bytes must be skept
            3)position based on which vox_offset is measured (in this case SEEK_SET=beginning of the file)
     Output: 0 in case of success, -1 in case of error*/
    ret= fseek(fp, (long)(hdr.vox_offset), SEEK_SET);
    if (ret !=0)
        printf("Error doinf fseek\n");


    //********** allocate buffer and read first 3D volume from data file*************/
    data[n] = (MY_DATATYPE *) malloc(sizeof(MY_DATATYPE) * hdr.dim[1]*hdr.dim[2]*hdr.dim[3]);

    if (data[n] == NULL)
            printf("Error allocating data buffer for %s\n.",hdr_file);

    ret = fread(data[n], sizeof(MY_DATATYPE), hdr.dim[1]*hdr.dim[2]*hdr.dim[3], fp);

    if (ret != hdr.dim[1]*hdr.dim[2]*hdr.dim[3])


    //****** scale the data buffer**********/
    /*If the scl_slope field is nonzero, then each voxel value in the dataset should be scaled as:
    y = scl_slope * x + scl_inter */
    if (hdr.scl_slope != 0) {
        for (i=0; i<hdr.dim[1]*hdr.dim[2]*hdr.dim[3]; i++)
            data[n][i] = (data[n][i] * hdr.scl_slope) + hdr.scl_inter;
    }
    fclose(fp);
    n++;

    //the number (n) of the current 3D image loaded in data matrix is returned
    return(n);
}


/*ones_mat: function to initialize the elements of a matrix to one*/
void ones_mat(double **one_mat, int r, int c) {
    int i, j;
    for (i=0;i<r;i++)
        for(j=0;j<c;j++)
            one_mat[i][j]=1.0;
}


/*save_bmp_image: function to save images in .bmp format*/
void save_bmp_image(unsigned char img[HEIGHT][WEIGHT][3], int nc, char *directories){
 
    std::stringstream out;
    out << nc;
    string path = out.str();
    path = directories + path + "_component.bmp";
    int w = WEIGHT;
    int h = HEIGHT;

    FILE *f;
    int filesize = 54 + 3*w*h;
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);


    f = fopen(&path[0],"wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);

    fwrite(img, 3, w*h, f);

    fclose(f);
}

/*set_range_after_smoothing: function to set parameters after smoothing */
void set_range_after_smoothing(vector<vector<double> > &slice, int d_x, int d_y, int indfinal[], int ind_in){
    int i, j;
    double max = -1000000;


    for(i=0;i<d_y;i++)
        {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                if(slice[i][j] > max) max = slice[i][j];
            }
        }
    }

    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                slice[i][j] = ((slice[i][j])/(max))*255;
            }
        }
    }
}


/*show_comp: function to filter and save images*/
void show_comp(int d_x, int d_y, int d_z, double **source, int indfinal[], vector<MY_DATATYPE> &brain, char *path){

        unsigned char img[HEIGHT][WEIGHT][3];
        vector<vector<double> > slice(d_y, vector<double>(d_x));
        int i, j, k, m, n, ind;

        for(k=0;k<NCOMP;k++)
        {
            // Initialization of a component
            for(i=0;i<HEIGHT;i++)
            {
                for(j=0;j<WEIGHT;j++)
                {
                    for(ind=0;ind<3;ind++) img[i][j][ind] = 0;
                }
            }
            ind = 0;

            // Operations on one component
            for(i=0;i<d_z/8;i++)
            {
                for(j=0;j<d_z/4;j++)
                {
                    // Operations on one slice of a component
                    for(m=0;m<d_y;m++)
                    {
                        for(n=0;n<d_x;n++)
                        {
                            if(indfinal[(i * d_z/4 + j) * d_x*d_y + m*d_x + n])
                            {
                                slice[m][n] = source[k][ind];
                                ind++;
                            }
                            else
                            {
                                slice[m][n] = 0;
                            }
                        }
                    }

                    // Extracting the most high value from the statistical analysis
                    limit_values(slice, d_x, d_y, indfinal, (i * d_z/4 + j) * d_x*d_y);

                    // Smoothing filter using a 10 gaussian window
                    smoothing_10(d_x, d_y, slice);

                    // Limiting the values between 0 and 255
                    set_range_after_smoothing(slice, d_x, d_y, indfinal, (i * d_z/4 + j) * d_x*d_y);

                    // Filling the rigth position of the image with a slice of the brain
                    show_slice(img, 17 + (130 * j), 506 - 80 - (140 * i), slice, d_x, d_y, (i * d_z/4 + j), brain);
                }
            }
            save_bmp_image(img, k+1,path);
            printf("\nSaved image of component n. %d",k);
        }
}


/*show_slice: function to extract slices*/
void show_slice(unsigned char img[HEIGHT][WEIGHT][3], int x_start, int y_start, vector<vector<double> > &slice, int d_x, int d_y, int z, vector<MY_DATATYPE> &brain){
    int i, j;

    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            if(slice[i][j]>50)
            {
                img[y_start + i][x_start + j][0] = 0;
                img[y_start + i][x_start + j][1] = round(slice[i][j]);
                img[y_start + i][x_start + j][2] = 255;
            }
            else
            {
                img[y_start + i][x_start + j][0] = brain[z*d_x*d_y + i*d_y + j];
                img[y_start + i][x_start + j][1] = brain[z*d_x*d_y + i*d_y + j];
                img[y_start + i][x_start + j][2] = brain[z*d_x*d_y + i*d_y + j];
            }
        }
    }
}


/*smoothing_10: function to get smoothed images */
void smoothing_10(int d_x, int d_y, vector<vector<double> > &slice){
    double gauss_window[10][10] = {{0.0019,    0.0066,    0.0167,    0.0310,    0.0423,    0.0423,    0.0310,    0.0167,    0.0066,    0.0019},{0.0066,    0.0228,    0.0576,    0.1067,    0.1453,    0.1453,    0.1067,    0.0576,    0.0228,    0.0066},{0.0167,    0.0576,    0.1453,    0.2694,    0.3667,    0.3667,    0.2694,    0.1453,    0.0576 ,   0.0167},{0.0310,    0.1067,    0.2694,    0.4994,    0.6799,    0.6799,    0.4994,    0.2694,    0.1067 ,   0.0310},{0.0423,    0.1453,    0.3667,    0.6799,    0.9257,    0.9257,    0.6799,    0.3667,    0.1453 ,   0.0423},{0.0423,    0.1453,    0.3667,    0.6799,    0.9257,    0.9257,    0.6799,    0.3667,    0.1453 ,   0.0423},{0.0310,    0.1067,    0.2694,    0.4994,    0.6799,    0.6799,    0.4994,    0.2694,    0.1067 ,   0.0310},{0.0167,    0.0576,    0.1453,    0.2694,    0.3667,    0.3667,   0.2694 ,   0.1453 ,   0.0576  ,  0.0167},{0.0066,    0.0228,    0.0576,    0.1067,    0.1453,    0.1453,   0.1067 ,   0.0576 ,   0.0228  ,  0.0066},{0.0019,    0.0066,    0.0167,    0.0310,    0.0423,    0.0423,   0.0310 ,   0.0167 ,   0.0066  ,  0.0019}};

    double sum_window;
    double sum;

    int i, j, m, n;
    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            sum_window = 0;
            sum = 0;
            for(m=-4;m<5;m++)
            {
                for(n=-4;n<5;n++)
                {
                    if(i+m >= 0 and i+m <d_y and j+n >= 0 and j+n <d_x)
                    {
                        sum += slice[i+m][j+n] * gauss_window[m+4][n+4];
                        sum_window += gauss_window[m+4][n+4];
                    }
                }
            }
            slice[i][j] = sum/sum_window;
        }
    }
}



/*vett_from_mat: function to get a column vector from the input matrix*/
void vett_from_mat(double **vett, int r_vett, double **mat, int ind){  //ottengo vett colonna
    int i;
    for (i=0;i<r_vett;i++)
        vett[i][0]=mat[i][ind];
}

/*zeros_mat: function to initialize the elements of the input matrix to zero*/
void zeros_mat(double **mat, int r, int c) {
    int i,j;

    for (i=0;i<r;i++)
        for(j=0;j<c;j++)
            mat[i][j]=0.0;
}

/*get_matrix_mult: function to calculate the multiplication of two matrices.
 The output matrix has dimensions: r1 x c2.*/
void get_matrix_mult_whiteM(double **a1, double **a2, int r1, int c1, int c2, my_double **a_mult){
int i, j, k;

    //Initializing the elements of the output matrix to zero
    zeros_mat_f(a_mult,r1,c2);

    for(i=0; i<r1; i++ ){
        for(j=0; j<c2; j++){
            for(k=0; k<c1; k++){
                a_mult[i][j] = a_mult[i][j] + a1[i][k] * a2[k][j];

            }
        }

    }
}
