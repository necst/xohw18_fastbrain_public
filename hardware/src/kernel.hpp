/*
 * kernel.hpp
 *
 *  Created on: Jun 20, 2018
 *      Author: filippo.carloni
 */

#ifndef SRC_KERNEL_HPP_
#define SRC_KERNEL_HPP_

typedef int my_int;
typedef float my_double;
typedef char my_char;


extern "C" void kernel( my_double* Wmat, my_double *whiteMat, my_double *white_mat_t, int *failure_value);

#endif /* SRC_KERNEL_HPP_ */
