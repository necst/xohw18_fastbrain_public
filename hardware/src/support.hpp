
#ifndef SUPPORT_H_
#define SUPPORT_H_

#include <sys/time.h>

double get_time()
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec + tv.tv_usec * 1e-6;
}

#endif
