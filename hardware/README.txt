# README #

This readme describes the steps necessary to run our FastBRaIn hardware implementation: 


#launch an f1 instance on AWS, we chose the FPGA development AMI version 1.3.5 since we synthesized using SDx 2017.1.op. We selected the instance f1.2xlarge

#after starting the f1 instance, upload on the AWS machine the necessary data to evaluate FastBRaIn, namely: images.nii.tar.gz, software and hardware folder

#The awsclbin is not included in the repo, hence the user must first create an Amazon FPGA Image (AFI) from the bitstream to create the awsxclbin

#A bitstream of our project can be found at this link: https://www.dropbox.com/sh/41giq7kxk2hh60m/AAC9aI-Jh6L1ZwBSYkr7E9lWa?dl=0



git clone https://github.com/aws/aws-fpga.git $AWS_FPGA_REPO_DIR

cd $AWS_FPGA_REPO_DIR

git checkout v1.3.6d

source sdaccel_setup.sh

sudo cp -rf SDAccel/aws_platform/ /opt/Xilinx/SDx/2017.1.op/platforms/ #in case we also want to compile/build our design

cd

sudo yum install lapack-devel blas-devel lapacke-devel #install dependencies


#clone our repo
#navigate to the folder containing your files

#running on fpga

#building host

cd hardware

#create the folder components, if it does not exist

#in case you need to build the host
source /opt/Xilinx/SDx/2017.1.op/settings64.sh

make host target=hw

sudo sh

source /opt/Xilinx/SDx/2017.1.rte.1ddr/setup.sh

./hw/xilinx_aws-vu9p-f1_4ddr-xpr-2pr_4.0/host_sw kernel.awsxclbin ../images.nii/nomi.txt ../images.nii/ components/