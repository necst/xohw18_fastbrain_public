# README #


Team number: xohw18-405

Project name: FastBRaIn: FastICA acceleration for Brain Resting-state networks Identification

Date: 30th June 2018

Version of uploaded archive: version 1

University name: Politecnico di Milano

Supervisor name: Professor Marco Domenico Santambrogio

Supervisor e-mail: marco.santambrogio@polimi.it

Participant: Filippo Carloni

Email: filippo.carloni@mail.polimi.it

Participant: Giada Casagrande

Email: giada.casagrande@mail.polimi.it

Participant: Valentina Corbetta

Email: valentina.corbetta@mail.polimi.it

Version of SDAccel: SDx 2017.1.op.

Amazon Instance: f1.2xlarge

Brief description of project:

FastBRaIn is a hardware implementation on FPGA for acceleration of FastICA algorithm to identify Resting State Networks, analyzing fMRI images. In particular, FastBRaIn has been implemented exploiting amazon F1 Instances. FastBRaIn has been developed at NECSTLab, Politecnico di Milano.

- Folder: 'hardware' contains 

          - IP core: kernel.cpp
		  
		  - header of kernel: kernel.hpp
		  
		  - SDAccel code: main-cl.cpp
		  
		  - header for loading fMRI images in NIfTI format: nift1.h 
		  
		  - file containing names of fMRI images in NIfTI format: nomi.txt
		  
		  - header of main: main.hpp
		  
		  The steps necessary to run the hardware implementation of FastBRaIn are described in the file README.txt, contained in this folder.
		  
- Folder: 'software' contains 

		  - C++ software implementation: FastICA_C.c
		  
		  - header for loading fMRI images in NIfTI format: nift1.h
		  
		  The steps necessary to run the software implementation of FastBRaIn are described in the file README.txt, contained in this folder.
		  
- File images.nii.tar.gz contains fMRI images in NIfTI format.

