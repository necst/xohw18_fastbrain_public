# README #

This readme describes the steps necessary to run our FastBRaIn software implementation

#install dependencies
sudo yum install lapack-devel blas-devel lapacke-devel 

To run the software implementation of FastBRaIn it is necessary to install the lapack library, which can be found at: http://www.netlib.org/lapack/

#clone our repo

#navigate to the folder containing your files

tar -xvf images.nii.tar.gz

cd software

make

# create the folder components, if it does not exist


#compile 
g++ -std=c++11 fastICA_C.c -llapacke -llapack -lblas -lm -O3 - o fastICA

#run
./fastICA ../images.nii/nomi.txt ../images.nii/ components/

