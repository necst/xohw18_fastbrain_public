/*

FastBRaIn is an implementation of FastICA to identify Resting State Networks, analyzing fMRI images.
FastBRaIn has been developed at NECSTLab, Politecnico di Milano.

Filippo Carloni:    filippo.carloni@mail.polimi.it
Giada Casagrande:   giada.casagrande@mail.polimi.it
Valentina Corbetta: valentina.corbetta@mail.polimi.it

*/

#include <complex.h>
#include <float.h>
#include <iostream>
#include <lapacke/lapacke.h>
#include <malloc.h>
#include <math.h>
#include "nifti1.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <time.h>
#include <vector>

using namespace std;

#define DIM 250 //dim represents the number of fMRI temporal scans (3D images in NIfTI file format). 
#define HEIGHT 512
#define MIN_HEADER_SIZE 348
#define NCOMP 84      //NCOMP: Setting number of statistically significant components
#define NII_HEADER_SIZE 352
#define WEIGHT 1024


typedef float  my_double;
typedef uint16_t MY_DATATYPE;

void acquire_brain(vector<MY_DATATYPE> &brain, MY_DATATYPE **data, int d1, int d2, int d3, int dim);
void get_diag (my_double **mat_diag, my_double *mat, int r, int c);
void get_eigvec (my_double **eigvec, my_double **p_eigvec, int r, int c);
void get_linear_img (int dim, int* indfinal, my_double **linear_img, int d1, int d2, int d3, MY_DATATYPE **data, int count);
int get_mask(my_double *xtemp, MY_DATATYPE **data,int dim, int d1, int d2, int d3, int *indfinal);
void get_matrix_mult(my_double **a1, my_double **a2, int r1, int c1, int c2, my_double **a_mult);
void get_mean (my_double **mat, int r_mat, int c_mat, my_double **vett_col);
void get_sqrt (my_double **a, my_double **a_sqrt, int r, int c );
void get_transp(my_double **a, my_double **aT, int r_a, int c_a);
void limit_values(vector<vector<my_double> > &slice, int d_x, int d_y, int indfinal[], int ind_in);
int load_nii (char *hdr_file, int n, int dim, MY_DATATYPE **data, int *dim1, int *dim2, int *dim3);
void ones_mat(my_double **one_mat, int r, int c);
void save_bmp_image( char *t,unsigned char img[HEIGHT][WEIGHT][3], int nc);
void set_range_after_smoothing(vector<vector<my_double> > &slice, int d_x, int d_y, int indfinal[], int ind_in);
void show_comp(int d_x, int d_y, int d_z, my_double **source, int indfinal[], vector<MY_DATATYPE> &brain, char *t);
void show_slice(unsigned char img[HEIGHT][WEIGHT][3], int x_start, int y_start, vector<vector<my_double> > &slice, int d_x, int d_y, int z, vector<MY_DATATYPE> &brain);
void smoothing_10(int d_x, int d_y, vector<vector<my_double> > &slice);
void vett_from_mat(my_double **vett, int r_vett, my_double **mat, int ind);
void zeros_mat(my_double **mat, int r, int c);


int main (int argc, char **argv) {
    // argv[1] nomi.txt
    // argv[2] path to the folder containing the .nii files
    // argv[3] components/
    

    int i,j,x,y,k, n=0, d1, d2, d3, nmax, count, cont, len, val, index_vet;
    int *dim1=NULL, *dim2=NULL, *dim3=NULL, *indfinal=NULL;
    char *str1, *res;
    my_double temperr,tol, sdev, **mt, a, b, norm, *xtemp=NULL, **linear_img, **linear_imgT, **meanL, **covMatrix, **f_in, **f1_out, **f1_out_T, **W_parte1, **W_parte2, **whiteM_div;
    my_double **whiteningMatrix,  **whiteM,**eigval_sqrt,  **eigval_sqrt_inv, **f2_out, **one_mat,**one_matT ,**source1, **source2, **source, **sourceT, **meanS, **meanST;
    my_double **source_vett, **source_vettT, *W_p, **V, **eigval, **U_p, **W_diag, **eigvec, **eigvecT, **W, **WT,**W_mult, **wmem,**wmemT, **wtemp, **W_vett, **W_vett_j, **W_vettT, **W_p2, **skew;
    FILE *fd;
    MY_DATATYPE **data=NULL;
 

    /*============ Allocating memory ===========*/

    //Allocating the rows of the matrix of data: allocation of a vertical array made of 'DIM' cells of pointers
    data = (MY_DATATYPE **) malloc(sizeof(MY_DATATYPE *) * DIM);

    dim1 = (int*) malloc(sizeof(int)*1); /*dimension x of 3D image*/
    dim2 = (int*) malloc(sizeof(int)*1); /*dimension y of 3D image*/
    dim3 = (int*) malloc(sizeof(int)*1); /*dimension z of 3D image*/


    /*========== Reading .nii files ==========*/

    /*The fMRI images analyzed are in the NIfTI format, which is an Analyze-style data format,
     proposed by the NIfTI DFWG as a short-term measure to facilitate inter-operation
     of functional MRI data analysis software packages.*/

    /*In C and C++, the access to a file can be made using a pointer to FILE,
     which is the file containing the information about the fMRI images*/

    // in argv[1] file names of .nii files are saved:in this case there are 250 strings of 43 characters each
    fd=fopen(argv[1],"r"); 
    if (fd==NULL)
    //if the file does not exist the output is NULL
        printf("No .nii file opened.\n");

    for (i=0; i<DIM; i++) {
        //length of the string (name of the files in argv[1])
        str1 = (char*) malloc(sizeof(char)*44);

        /*fgets(char* str,int num,FILE* stream): function to read characters from stream and stores
        them as a C string into str until (num-1) characters have been read or either a newline or
        the end-of-file is reached.
        A newline character makes fgets stop reading, but it is considered a valid character by the
        function and included in the string copied to str.
        A terminating null character is automatically appended after the characters copied to str.*/
        res=fgets(str1,44, fd);
        if(res==NULL){
            return 0;
        }

	 /*========== Loading the 3D images ==========*/
        printf("Loading volume %d.\n",n);
        //Each 3D image is composed of dim1 x dim2 x dim3 pixels
        char path[200];
        strcpy(path,argv[2]);
        str1=strcat(path, str1);
        //Each 3D image is composed of dim1 x dim2 x dim3 pixels
        n=load_nii(str1, n, DIM, data, dim1, dim2, dim3);
        d1=*dim1;
        d2=*dim2;
        d3=*dim3;
    }
    fclose(fd);
    printf("End of images loading.\n");


    /*========== Starting preprocessing ==========*/

    /*=== masking 3d image ===*/
    /*It is necessary to identify a Region of Interest (ROI) and the background;
     to do so, a masking process is performed. First, the mean is calculated and only the
     elements with a value superior to the mean are kept. Second, the mean is subtracted from the remaining values,
     to centre the data*/

    len= d1*d2*d3;
    xtemp = (my_double *) malloc(sizeof(my_double)* len);
    indfinal = (int *) malloc(sizeof(int)* len);
    count=get_mask(xtemp, data, DIM, d1, d2, d3, indfinal);

    //The image is linearized to obtain a (DIM x count) matrix
    linear_img = (my_double **) malloc(sizeof(my_double *) * DIM);
    for (i=0;i<DIM; i++)
        linear_img[i]=(my_double*)malloc(sizeof(my_double)* count);

    get_linear_img(DIM, indfinal, linear_img,d1,d2,d3, data, count);

    //removing the mean: meanL is a column vector containing the mean at each temporal instant
    meanL =  (my_double **) malloc(sizeof(my_double*)* DIM);
    for (i=0;i<DIM; i++)
        meanL[i]=(my_double*)malloc(sizeof(my_double)*1);

    //calulating the mean at each temporal instant and subtracting it from the row of the corresponding time
    get_mean (linear_img, DIM , count, meanL);

    for(i=0;i<DIM; i++)
        for (j=0;j<count; j++)
            linear_img[i][j]=linear_img[i][j] - meanL[i][0];


    /*========== dimension reduction by PCA and whitening ==========*/

    /*Part of the preprocessing consists in data reduction, by performing the Principal Component Analysis (PCA),
     which reduces the number of random variables considered by obtaining a set of principal variables. This is
     done to eliminate the variables which do not contain relevant information.
     PCA can be used to perform the whitening of the data: after centering the data, the decomposition in eigenvectors
     and eigenvalues of the matrix of the covariance is performed.*/

    covMatrix= (my_double **) malloc(sizeof(my_double *)*DIM);
    for (i=0; i<DIM; i++)
        covMatrix[i] = (my_double *) malloc(sizeof(my_double )*DIM);

    linear_imgT = (my_double **) malloc(sizeof(my_double *) * count);
    for (i=0;i<count; i++)
        linear_imgT[i]=(my_double*)malloc(sizeof(my_double)* DIM);

    //The transpose of the linear image is needed, after the removal of the mean
    get_transp (linear_img, linear_imgT, DIM, count);

    //Function necessary to obtain the matrix of the covariance of linear_img
    get_matrix_mult(linear_img, linear_imgT, DIM, count, DIM, covMatrix);

    //Dividing every element of the matrix of the covariance for count
    for (i=0; i<DIM; i++)
        for (j=0; j<DIM; j++)
            covMatrix[i][j]=covMatrix[i][j] / (count);


    //Allocating memory for the matrices of eigenvalues and eigenvectors
    W_p=(my_double*) malloc(sizeof(my_double)*DIM);

    U_p= (my_double **) malloc(sizeof(my_double *)*DIM);
    for (i=0; i<DIM; i++)
        U_p[i] = (my_double *) malloc(sizeof(my_double )*DIM);

    eigvec= (my_double **) malloc(sizeof(my_double *)*DIM);
    for (i=0; i<DIM; i++)
        eigvec[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    eigvecT= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigvecT[i] = (my_double *) malloc(sizeof(my_double )*DIM);

    W_diag = (my_double **) malloc(sizeof(my_double *)*DIM);
    for (i=0; i<DIM; i++)
        W_diag[i] = (my_double *) malloc(sizeof(my_double )*DIM);

    eigval = (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigval[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);



    //Using LAPACKE (an interface to use LAPACK from C) libraries and functions to calculate eigenvalues and eigenvectors
    lapack_int nl = DIM, lda= DIM, info;
    double cov_lapack[nl*nl], eigvalues[nl];

    //Coping elements of covMatrix in cov_lapack
    for (i=0;i<DIM;i++)
    	for(j=0;j<DIM;j++)
    		cov_lapack[DIM*i+j]=covMatrix[i][j];

    /*LAPACKE_dsyev computes all eigenvalues and eigenvectors of an nl-by-nl real symmetric matrix
        input: 1) LAPACK_ROW_MAJOR: interpretation of memory as row-major
               2) 'V':  computes eigenvalues and eigenvectors
               3) 'U':  upper triangle of the real symmetric matrix is stored
               4) nl: the order of the matrix cov_lapack (nl>=0)
               5) cov_lapack: real symmetric matrix (it is overwritten with eigenvectors matrix)
               6) lda: the leading dimension of the array ( lda>=max(1,nl))
               7) eigvalues: vector of eigenvalues in ascending order
        output: info
                check for convergence:
                     = 0:  successful exit
                     < 0:  if info=-i, the i-th argument had an illegal value
                     > 0:  if info=i, the algorithm failed to converge; i off-diagonal elements of an intermediate tridiagonal
                          form did not converge to zero
        */
    info=LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'V', 'U', nl, cov_lapack, lda, eigvalues);
    if (info!=0) {
    	printf("The algorithm failed");
    	return 0;
    }

    //Coping the vector of eigenvalues in W_p, sorting them in descending order
    for(i=0;i<nl;i++)
        W_p[nl-1-i]=eigvalues[i];

    //Diagonalization of W_p to obtain W_diag
    get_diag (W_diag, W_p, DIM, DIM);
    //Reduction of W_diag to eigval, a diagonal matrix containing the NCOMP most significant eigenvalues
    for (i=0;i<NCOMP; i++)
        for (j=0;j<NCOMP; j++)
            eigval[i][j]=W_diag[i][j];

    //Coping the eigenvectors obtained using LAPACKE_dsyev in U_p, sorting also columns
    for (i=0;i<nl;i++)
        for(j=0;j<nl;j++)
            U_p[i][nl-1-j]=cov_lapack[i*nl+j];

    //Getting eigvec matrix of eigenvectors corresponding to the NCOMP most significant eigenvalues saved
    get_eigvec (eigvec, U_p, DIM, NCOMP);


    whiteningMatrix= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        whiteningMatrix[i] = (my_double *) malloc(sizeof(my_double )*DIM);


    whiteM= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        whiteM[i] = (my_double *) malloc(sizeof(my_double )*count);

    whiteM_div= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        whiteM_div[i] = (my_double *) malloc(sizeof(my_double )*count);

    eigval_sqrt= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigval_sqrt[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    eigval_sqrt_inv= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        eigval_sqrt_inv[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);


    get_transp(eigvec, eigvecT, DIM, NCOMP);
    get_sqrt(eigval, eigval_sqrt, NCOMP, NCOMP);


    for (i=0;i<NCOMP; i++)
        for (j=0;j<NCOMP; j++){
            if(eigval_sqrt[i][j])
                eigval_sqrt_inv[i][j]= 1.0/eigval_sqrt[i][j];
            else
                eigval_sqrt_inv[i][j]= eigval_sqrt[i][j];
        }


    //Obtaining the whitened matrix
    get_matrix_mult(eigval_sqrt_inv, eigvecT, NCOMP, NCOMP, DIM, whiteningMatrix);
    get_matrix_mult(whiteningMatrix, linear_img, NCOMP, DIM , count , whiteM);


   /* ========== End of preprocessing ==========*/

   //initialization of parameters needed in FastICA
    nmax=1000;  //maximum number of iterations
    tol=0.0001; //tolerance

    //Allocating memory for the variables needed in FastICA
    W= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    WT= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        WT[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    wmem= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        wmem[i] = (my_double *) malloc(sizeof(my_double )*1);

    wmemT= (my_double **) malloc(sizeof(my_double *)*1);
    for (i=0; i<1; i++)
        wmemT[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    wtemp= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        wtemp[i] = (my_double *) malloc(sizeof(my_double )*1);

    W_vett= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_vett[i] = (my_double *) malloc(sizeof(my_double )*1);

    W_vett_j= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_vett_j[i] = (my_double *) malloc(sizeof(my_double )*1);

    W_vettT= (my_double **) malloc(sizeof(my_double *)*1);
    for (i=0; i<1; i++)
        W_vettT[i] = (my_double *) malloc(sizeof(my_double )*NCOMP);

    W_mult= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_mult[i] = (my_double *) malloc(sizeof(my_double )*DIM);

    f_in= (my_double **) malloc(sizeof(my_double *)*1);
    for (i=0; i<1; i++)
        f_in[i] = (my_double *) malloc(sizeof(my_double )*count);

    f1_out= (my_double **) malloc(sizeof(my_double *)*1);
    for (i=0; i<1; i++)
        f1_out[i] = (my_double *) malloc(sizeof(my_double )*count);

    f1_out_T= (my_double **) malloc(sizeof(my_double *)*count);
    for (i=0; i<count; i++)
        f1_out_T[i] = (my_double *) malloc(sizeof(my_double )*1);

    W_parte1= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_parte1[i] = (my_double *) malloc(sizeof(my_double )*1);

    W_parte2= (my_double **) malloc(sizeof(my_double *)*NCOMP);
    for (i=0; i<NCOMP; i++)
        W_parte2[i] = (my_double *) malloc(sizeof(my_double )*1);

    f2_out = (my_double**) malloc(sizeof(my_double*)*1);
    for (x=0;x<1;x++)
        f2_out [x]=(my_double*)malloc(sizeof(my_double)*count);

    one_mat = (my_double**) malloc(sizeof(my_double*)*count);
    for (x=0;x<count;x++)
            one_mat [x]=(my_double*)malloc(sizeof(my_double)*1);

    one_matT = (my_double**) malloc(sizeof(my_double*)*1);
    for (x=0;x<1;x++)
            one_matT [x]=(my_double*)malloc(sizeof(my_double)*count);

    W_p2= (my_double**) malloc(sizeof(my_double*)*1);
    for (x=0;x<1;x++)
            W_p2[x]=(my_double*)malloc(sizeof(my_double)*1);

    source1= (my_double**) malloc(sizeof(my_double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            source1[x]=(my_double*)malloc(sizeof(my_double)*count);

    source2= (my_double**) malloc(sizeof(my_double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            source2[x]=(my_double*)malloc(sizeof(my_double)*count);

    source= (my_double**) malloc(sizeof(my_double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            source[x]=(my_double*)malloc(sizeof(my_double)*count);

    sourceT= (my_double**) malloc(sizeof(my_double*)*count);
    for (x=0;x<count;x++)
            sourceT[x]=(my_double*)malloc(sizeof(my_double)*NCOMP);

    meanS= (my_double**) malloc(sizeof(my_double*)*1);
    for (x=0;x<1;x++)
            meanS[x]=(my_double*)malloc(sizeof(my_double)*NCOMP);

    meanST= (my_double**) malloc(sizeof(my_double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            meanST[x]=(my_double*)malloc(sizeof(my_double)*1);

    skew= (my_double**) malloc(sizeof(my_double*)*NCOMP);
    for (x=0;x<NCOMP;x++)
            skew[x]=(my_double*)malloc(sizeof(my_double)*1);

    //A matrix full of random numbers is needed to perform FastICA
    srand(time(NULL));
    a = 0.5;
    for (i=0;i<NCOMP;i++)
        for (j=0;j<NCOMP;j++)
            //Limiting rand function to obtain only float numbers between 0 and 1
            W[i][j]=(float)(rand()/(float)(RAND_MAX)) * a;


   //Normalization of the column vectors of W
    for (i=0;i<NCOMP; i++) {
        norm=0.0;
        for (x=0;x<NCOMP;x++)
            norm=norm + W[x][i]*W[x][i];
        norm=sqrt(norm);
        for (x=0;x<NCOMP; x++)
            W[x][i]=W[x][i]/norm;
    }



    //---------------------------------- Starting fastICA ------------------------------//

    /*FastICA is a popular algorithm to perform Independent Component Analysis. It was developed
     by Aapo Hyvarinen at Helsinki University of Technology. FastICA seeks an orthogonal rotation
     of the prewhitened data, through a fixed point iteration scheme, that maximizes a measure of non-Gaussianity of the
     rotated components.*/

    // non linear functions needed
    // f1(x) = tanh(x)
    // f2(x) = 1-(tanh(x))^2 (first derivative of the above function)

    // Giving an event to start time execution calculation 
    clock_t launch = clock();
    
    for (i=0; i<NCOMP; i++) {
        // variables 'temperr' is inizialized to 1 and 'count' to 0 to ensure at least one iteration in the following for loop 
        temperr=1.0;
        cont=0;
        // 'wmem' is inizialized to zeros
        zeros_mat(wmem, NCOMP, 1);
        printf("component number %d\n", i);
        while(cont<nmax && temperr>tol) {
            cont++;
            
            // The i-th column from random matrix 'W' is extracted and saved in vector 'W_vett'. Then the transposed vector is calculated  
            // and the multiplication between vector 'W_vettT' and matrix 'whiteM' is executed to obtain the argument (f1_in) of non-linear function f1
            vett_from_mat(W_vett,NCOMP,W, i);
            get_transp(W_vett, W_vettT, NCOMP, 1);               
            get_matrix_mult(W_vettT, whiteM, 1, NCOMP, count, f_in);

            // Calculation of 'tanh(f_in)' to obtain the output (f1_out) of non-linear function f1
            for (x=0;x<count; x++)
                f1_out[0][x]=tanh(f_in[0][x]);

            // The transposed vector of 'f1_out' is calculated and multiplied by 'whiteM_div', that has been obtained dividing
            // the matrix 'whiteM' for 'NCOMP'. The result is the term 'W_parte1'. 
            get_transp(f1_out, f1_out_T, 1, count);
            for (x=0;x<NCOMP; x++)
                for (y=0;y<count; y++)
                    whiteM_div[x][y]=whiteM[x][y]*(1.0/ NCOMP);
            get_matrix_mult(whiteM_div, f1_out_T, NCOMP, count, 1, W_parte1);

            // The output ('f2_out') of non-linear function f2 is obtained by substracting the square of 'f1_out' from 1, then difference is divided for 'NCOMP' 
            for (x=0;x<count; x++)
                f2_out[0][x]=1.0 - (tanh(f_in[0][x])*tanh(f_in[0][x]));
            for (x=0;x<count; x++)
                    f2_out[0][x]=  f2_out[0][x] * (1.0/ NCOMP);
            
            // 'f2_out' is multiplied by a vector inizialized at 1 and the result is the number 'W_p2'. Result of multiplication between this number
            // and vector W_vett is substracted from 'W_parte1'. 
            ones_mat(one_mat, count, 1);
            get_matrix_mult( f2_out, one_mat, 1, count, 1, W_p2);
            for (x=0;x<NCOMP; x++)
                W_parte2[x][0]=W_vett[x][0]*W_p2[0][0];
            for (x=0;x<NCOMP; x++)
                W_vett[x][0]=W_parte1[x][0] - W_parte2[x][0];
            
            /* To prevent different vectors from converging to the same maxima we must decorrelate the outputs after every iteration.
            A way of achieving decorrelation is a deflation scheme based on a Gram-Schmidt-like decorrelation: this means that we estimate 
            the independent components one by one. When we have estimated i independent components, or i vectors w1 ,..., wi (that are the
            columns of matrix 'W'), we run the one-unit fixed-point algorithm for wi+1 , and after every iteration step it is necessary to
            subtract from wi+1 the “projections” wTi+1 * wj * wj, j = 1 ,..., i of the previously estimated i vectors, and then renormalize wi+1:
            1. Let: wi+1 = wi+1 − ∑ (wTi+1 * wj *wj), j=1,...,i
            2. Let: wi+1 = wi+1 / sqrt(wTi+1 * wi+1).
            */
            zeros_mat(wtemp, NCOMP, 1);
            get_transp(W_vett, W_vettT, NCOMP, 1);
            for (x=0;x<NCOMP; x++)
                W[x][i]=W_vett[x][0];
            for (j=0; j<i; j++) {
                vett_from_mat(W_vett_j,NCOMP,W, j);
                get_matrix_mult( W_vettT, W_vett_j, 1, NCOMP, 1, W_p2);
                for (x=0;x<NCOMP; x++)
                    wtemp[x][0]=wtemp[x][0]+ W_vett_j[x][0]*W_p2[0][0];
            }
            for (x=0;x<NCOMP; x++)
                W_vett[x][0]=W_vett[x][0]- wtemp[x][0];

            /*Orthonormalization of Gram-Schmit: it is an algorithm which helps to obtain a number of orthogonal
            vectors starting from a generic number of linearly independent vectors*/
            get_transp(W_vett, W_vettT, NCOMP, 1);
            get_matrix_mult( W_vettT, W_vett, 1, NCOMP, 1, W_p2);
            for (x=0;x<NCOMP; x++)
                W_vett[x][0]=W_vett[x][0]/sqrt(W_p2[0][0]);

            // Convergence error is calculated and saved in 'temperr' variable 
            get_transp(wmem, wmemT, NCOMP, 1);
            get_matrix_mult( wmemT, W_vett, 1, NCOMP, 1, W_p2);
            temperr= 1.0-fabs(W_p2[0][0]);

            // column vector 'W_vett' is saved in 'wmem'
            for (x=0;x<NCOMP;x++)
                wmem[x][0]=W_vett[x][0];
            
            // The old i-th column vector of matrix 'W' is overwritten with vector 'W_vett': i-th independent component has been calculated. 
            for (x=0;x<NCOMP; x++)
                W[x][i]=W_vett[x][0];
        }
        
        // If convergence is not reached in a number of iterations that is less than the maximum one defined as NMAX, a message is displayed
        if (cont==nmax)
            printf("No convergence, possible error.\n");
        // on the other hand the number of iteration is displayed.
        else
            printf("Convergence after %d steps.\n", cont);
    }

   // End of time execution calculation: time execution of FastIca algorithm is dislayed
    clock_t done = clock();
    double diff = (double)(done - launch) / (double)CLOCKS_PER_SEC;
    printf("\nfastICA_execution_time: %3.20lf s\n\n", diff);

    /*------------------------------- End of fastICA ------------------------------------*/


    //Adding mean back to the data
    get_transp(W, WT, NCOMP, NCOMP);
    get_matrix_mult( WT, whiteningMatrix, NCOMP, NCOMP, DIM, W_mult);
    get_matrix_mult( W_mult, linear_img, NCOMP, DIM, count, source1);
    get_transp(one_mat, one_matT, count, 1);
    get_matrix_mult( W_mult, meanL, NCOMP, DIM, 1, meanST);
    get_matrix_mult( meanST, one_matT, NCOMP , 1, count, source2);

    //Obtaining the source matrix after FastICA
    for (x=0;x<NCOMP; x++)
        for (y=0;y<count;y++)
            source[x][y]=source1[x][y]+source2[x][y];


    /*========== Choosing sign by skewness ==========*/

    /*Skewness is a measure of lack of simmetry in a statistical distribution; it helps to
    understand how much a distribution differs from a normal one*/

    //Obtaining the mean of source: meanST
    get_mean (source, NCOMP , count, meanST);
    get_transp(meanST, meanS, NCOMP, 1);
    zeros_mat(skew, NCOMP, 1);
    for (i=0;i<NCOMP; i++) {
        a=0.0;
        for (j=0; j<count; j++) {
            a=a+(my_double)pow((source[i][j] - meanS[0][i]),2.0);
            skew[i][0]=skew[i][0] + (my_double)pow((source[i][j] - meanS[0][i]),3.0);
        }
        b=sqrt(a/(my_double)count);
        b=(my_double)pow(b,3.0);
        skew[i][0]=(skew[i][0]/(my_double)count)/b;
    }

    for (i=0;i<NCOMP; i++)
        if (skew[i][0] <0)
            for(j=0;j<count;j++)
                source[i][j] = -source[i][j];


    /*========== Conversion to z-score ==========*/

    //Allocating memory
    source_vett=(my_double**)malloc(sizeof(my_double*)*1);
    for (i=0;i<1;i++)
        source_vett[i]=(my_double*)malloc(sizeof(my_double)*count);

    source_vettT=(my_double**)malloc(sizeof(my_double*)*count);
    for (i=0;i<count;i++)
        source_vettT[i]=(my_double*)malloc(sizeof(my_double)*1);

    mt=(my_double**)malloc(sizeof(my_double*)*1);
    for (i=0;i<1;i++)
        mt[i]=(my_double*)malloc(sizeof(my_double)*1);

    get_transp(source, sourceT,NCOMP, count);

    /*Calculating the standard deviation to quantify the amount of variation
    or dispersion of our set of data*/
    for (i=0;i<NCOMP; i++) {
        vett_from_mat(source_vettT, count, sourceT, i);
        get_transp(source_vettT, source_vett, count, 1);

        //Obtainig the standard deviation of each column
        get_mean(source_vett, 1 , count, mt);
        sdev=0.0;
        for (x=0;x<count; x++)
            sdev= sdev + pow(((source_vett[0][x]) - mt[0][0]), 2.0);
        sdev=sqrt(sdev/(my_double)count);
        for (x=0;x<count; x++)
            source[i][x]=((source_vett[0][x])-mt[0][0])/ ((my_double)sdev);
    }



    /*========== Saving components ==========*/

    printf("\nStart saving pictures...\n");

    // Extracting the background of the brain images to show the components
    vector<MY_DATATYPE> brain(len);
    acquire_brain(brain, data, d1, d2, d3, DIM);

    // Saving images of all the components
    show_comp(d1, d2, d3, source, indfinal, brain, argv[3]);

    printf("\n\nPictures saved\n");
    printf("End: final components obtained\n");


    return 0;
}


/*========== Functions used ==========*/

/*acquire_brain: function to get brain images for reconstruction of final fastICA components*/
void acquire_brain(vector<MY_DATATYPE> &brain, MY_DATATYPE **data, int d1, int d2, int d3, int dim){
    int len = d1*d2*d3;
    int i, j, k;
    my_double max, min, range;

    for(i=0;i<len;i++)
    {
        brain[i] = data[dim/2][i];
    }

    for(k=0;k<d3;k++)
    {
        min = 1000000;
        max = -1000000;
        for(j=0;j<d2*d1;j++)
        {
            if(brain[k*d2*d1 + j] < min) min = brain[k*d2*d1 + j];
            if(brain[k*d2*d1 + j] > max) max = brain[k*d2*d1 + j];
        }
        range = max - min;
        for(j=0;j<d2*d1;j++)
        {
            brain[k*d2*d1 + j] = round(((brain[k*d2*d1 + j] - min)/range)*255);
        }
    }
}

/*get_diag: function to obtain a matrix, which has the elements of the input vector
 on the diagonal and zeros in each other position.*/
void get_diag (my_double **mat_diag, my_double *mat, int r, int c) {
    int i, j;
    for (i=0;i<r; i++)
        for (j=0;j<c; j++){
            if (i==j)
                mat_diag[i][j]=mat[i];
            else
                mat_diag[i][j]=0;
        }
}


/*get_eigvec: function to save only the first c columns of the whole matrix of eigenvectors */
void get_eigvec (my_double **eigvec, my_double **p_eigvec, int r, int c){
    int i, j;
    for (i=0;i<r; i++)
        for (j=0;j<c; j++)
            eigvec[i][j]=p_eigvec[i][j];
}


/*get_linear_img: function to obtain masked linearized data*/
void get_linear_img (int dim, int* indfinal, my_double **linear_img, int d1, int d2, int d3, MY_DATATYPE **data,int count) {
    int i, j, k=0;

    for (i=0;i<dim; i++){
        k=0;
        for (j=0;j<d1*d2*d3;j++){
            if (indfinal[j]){
                linear_img[i][k]=data[i][j];
                k++;
            }
        }
    }
}


/*get_mask: function to create the mask in pre-processing*/
int get_mask(my_double *xtemp, MY_DATATYPE **data, int dim, int d1, int d2, int d3,  int *indfinal){
    int i, j, count=0, o;
    my_double sum=0, m;

    for (i=0; i<d1*d2*d3; i++)
        xtemp[i]=data[(dim/2)-1][i];

    //Obtaining the mean of vector xtemp
    for (i=0;i<d1*d2*d3; i++)
        sum=sum+xtemp[i];
    m=sum/(my_double)(d1*d2*d3);


    for (i=0;i<d1*d2*d3; i++){
        if (xtemp[i]>=m){
            // in indfinal vector, 1 is set when the corresponding value in xtemp is greater than or equal to the mean
            indfinal[i]=1;
            //counting how many times the previous condition is verified
            count++;
        }
        else
            // in indfinal vector, 0 is set when the corresponding value in xtemp is less than the mean
            indfinal[i]=0;
    }

    //it returns how many 1 there are in indfinal, which represents the number of columns in linear_img
    return count;
}



/*get_matrix_mult: function to calculate the multiplication of two matrices.
 The output matrix has dimensions: r1 x c2.*/
void get_matrix_mult(my_double **a1, my_double **a2, int r1, int c1, int c2, my_double **a_mult) {
    int i, j, k;

    //Initializing the elements of the output matrix to zero
    zeros_mat(a_mult,r1,c2);

    for(i=0; i<r1; i++ ){
        for(j=0; j<c2; j++){
            for(k=0; k<c1; k++){
                a_mult[i][j] = a_mult[i][j] + a1[i][k] * a2[k][j];

            }
        }

    }

}


/*get_mean: function to calculate the mean of each row of a matrix.
 The output is a column vector, containing the mean of the rows.*/
void get_mean (my_double **mat, int r_mat, int c_mat, my_double **vett_col) {
    int i, j;
    my_double sum;

    for (i=0;i<r_mat; i++) {
        sum=0;
        for (j=0; j<c_mat; j++)
            sum=sum+mat[i][j];
        vett_col[i][0]=sum/(my_double)c_mat;

    }
 }

 /*get_sqrt: function to save the square root of the elements of the input matrix
 in the output matrix*/
void get_sqrt (my_double **a, my_double **a_sqrt, int r, int c ) {
    int i, j;
    for (i=0;i<r;i++)
        for (j=0;j<c;j++)
            a_sqrt[i][j]=sqrt(fabs(a[i][j]));
}

/*get_transp: function to obtain the transposed matrix of the input matrix*/
void get_transp(my_double **a, my_double **aT, int r_a, int c_a){
    int i, j;
    for (i=0;i<r_a;i++)
        for(j=0;j<c_a;j++)
            aT[j][i]=a[i][j];
}


/*limit_values: function to set parameters for image reconstruction*/
void limit_values(vector<vector<my_double> > &slice, int d_x, int d_y, int indfinal[], int ind_in){
    int i, j;
    my_double max = -1000000;
    my_double lim_val = 3;

    for(i=0;i<d_y;i++)
        {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                if(slice[i][j] > lim_val)
                {
                    if(slice[i][j] > max) max = slice[i][j];
                }
                else
                {
                    slice[i][j] = lim_val;
                }
            }
        }
    }

    my_double range = max - lim_val;

    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                slice[i][j] = ((slice[i][j]-lim_val)/(range))*255;
            }
        }
    }
}


/*load_nii:function to save a volume in an array, linearized --> array: dim1 x dim2 x dim3
*hdr_file: is a pointer to the string where the name of the .nii file is stored;
n: is the number of the current volume*/
int load_nii (char *hdr_file,int n, int dim, MY_DATATYPE **data, int *dim1, int *dim2, int *dim3){

    nifti_1_header  hdr;
    FILE *fp;
    int ret,i,j,k;
    my_double total;

    /*========= read_nifti_file ==========*/
    //***open and read hdr_file***/

    // fopen: function to open the file saved in hdr_file, with a read mode
    fp= fopen(hdr_file, "r");
    if (fp==NULL)
        printf("No nii file opened.\n");

    /*fread: function to read the elements in a vector from a stream, in this case from fd.
    Input: 1)address in which data are saved
           2)dimension of each element in byte (MIN_HEADER_SIZE = 348)
           3)number of elements that must be read
           4)pointer to the file from which data are read
    Output: the output value must be equal to the third input, otherwise an error has occured*/
    ret= fread(&hdr, MIN_HEADER_SIZE, 1, fp);
    if (ret != 1)
        printf("No file has been read\n");

    //****** print header info ******/
    //  printf("%s header information\n", hdr_file);
    //  printf("array dimensions: %d %d %d %d \n", hdr.dim[1], hdr.dim[2], hdr.dim[3], hdr.dim[4]);
    //  printf("Datatype and bits/pixel: %d %d \n", hdr.datatype, hdr.bitpix);
    //  printf("Scaling slope and intercept: %.6f %.6f\n", hdr.scl_slope, hdr.scl_inter);
    //  printf("Byte offset to data in datafile: %ld \n", (long)(hdr.vox_offset));

    //A cast is needed to get an int, since hdr.dim is of type short
    *dim1=(int)hdr.dim[1];
    *dim2=(int)hdr.dim[2];
    *dim3=(int)hdr.dim[3];

    //****** read hdr_file and jump to data offset ******/
    /*fseek: function to set the index of the position of the file. The next operation of I/O on stream will
     be executed from the new position that has been set. The position is obtained adding offset,
     which can also be a negative value, to SEEK_SET.
     Input: 1)file from which the data are read
            2)how many bytes must be skept
            3)position based on which vox_offset is measured (in this case SEEK_SET=beginning of the file)
     Output: 0 in case of success, -1 in case of error*/
    ret= fseek(fp, (long)(hdr.vox_offset), SEEK_SET);
    if (ret !=0)
        printf("Error doinf fseek\n");


    //********** allocate buffer and read first 3D volume from data file*************/
    data[n] = (MY_DATATYPE *) malloc(sizeof(MY_DATATYPE) * hdr.dim[1]*hdr.dim[2]*hdr.dim[3]);

    if (data[n] == NULL)
            printf("Error allocating data buffer for %s\n.",hdr_file);

    ret = fread(data[n], sizeof(MY_DATATYPE), hdr.dim[1]*hdr.dim[2]*hdr.dim[3], fp);

    if (ret != hdr.dim[1]*hdr.dim[2]*hdr.dim[3])


    //****** scale the data buffer**********/
    /*If the scl_slope field is nonzero, then each voxel value in the dataset should be scaled as:
    y = scl_slope * x + scl_inter */
    if (hdr.scl_slope != 0) {
        for (i=0; i<hdr.dim[1]*hdr.dim[2]*hdr.dim[3]; i++)
            data[n][i] = (data[n][i] * hdr.scl_slope) + hdr.scl_inter;
    }
    fclose(fp);
    n++;

    //the number (n) of the current 3D image loaded in data matrix is returned
    return(n);
}


/*ones_mat: function to initialize the elements of a matrix to one*/
void ones_mat(my_double **one_mat, int r, int c) {
    int i, j;
    for (i=0;i<r;i++)
        for(j=0;j<c;j++)
            one_mat[i][j]=1.0;
}


/*save_bmp_image: function to save images in .bmp format*/
void save_bmp_image(char* t,unsigned char img[HEIGHT][WEIGHT][3], int nc){
    string path = to_string(nc);
    path = t + path + "_component.bmp"; 
    int w = WEIGHT;
    int h = HEIGHT;

    FILE *f;
    int filesize = 54 + 3*w*h;
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);


    f = fopen(&path[0],"wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);

    fwrite(img, 3, w*h, f);

    fclose(f);
}

/*set_range_after_smoothing: function to set parameters after smoothing */
void set_range_after_smoothing(vector<vector<my_double> > &slice, int d_x, int d_y, int indfinal[], int ind_in){
    int i, j;
    my_double max = -1000000;


    for(i=0;i<d_y;i++)
        {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                if(slice[i][j] > max) max = slice[i][j];
            }
        }
    }

    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            if(indfinal[ind_in + i*d_x + j])
            {
                slice[i][j] = ((slice[i][j])/(max))*255;
            }
        }
    }
}


/*show_comp: function to filter and save images*/
void show_comp(int d_x, int d_y, int d_z, my_double **source, int indfinal[], vector<MY_DATATYPE> &brain, char *t){

        unsigned char img[HEIGHT][WEIGHT][3];
        vector<vector<my_double> > slice(d_y, vector<my_double>(d_x));
        int i, j, k, m, n, ind;

        for(k=0;k<NCOMP;k++)
        {
            // Initialization of a component
            for(i=0;i<HEIGHT;i++)
            {
                for(j=0;j<WEIGHT;j++)
                {
                    for(ind=0;ind<3;ind++) img[i][j][ind] = 0;
                }
            }
            ind = 0;

            // Operations on one component
            for(i=0;i<d_z/8;i++)
            {
                for(j=0;j<d_z/4;j++)
                {
                    // Operations on one slice of a component
                    for(m=0;m<d_y;m++)
                    {
                        for(n=0;n<d_x;n++)
                        {
                            if(indfinal[(i * d_z/4 + j) * d_x*d_y + m*d_x + n])
                            {
                                slice[m][n] = source[k][ind];
                                ind++;
                            }
                            else
                            {
                                slice[m][n] = 0;
                            }
                        }
                    }

                    // Extracting the most high value from the statistical analysis
                    limit_values(slice, d_x, d_y, indfinal, (i * d_z/4 + j) * d_x*d_y);

                    // Smoothing filter using a 10 gaussian window
                    smoothing_10(d_x, d_y, slice);

                    // Limiting the values between 0 and 255
                    set_range_after_smoothing(slice, d_x, d_y, indfinal, (i * d_z/4 + j) * d_x*d_y);

                    // Filling the rigth position of the image with a slice of the brain
                    show_slice(img, 17 + (130 * j), 506 - 80 - (140 * i), slice, d_x, d_y, (i * d_z/4 + j), brain);
                }
            }
            save_bmp_image(t,img, k+1);
            printf("\nSaved image of component n. %d",k);
        }
}


/*show_slice: function to extract slices*/
void show_slice(unsigned char img[HEIGHT][WEIGHT][3], int x_start, int y_start, vector<vector<my_double> > &slice, int d_x, int d_y, int z, vector<MY_DATATYPE> &brain){
    int i, j;

    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            if(slice[i][j]>50)
            {
                img[y_start + i][x_start + j][0] = 0;
                img[y_start + i][x_start + j][1] = round(slice[i][j]);
                img[y_start + i][x_start + j][2] = 255;
            }
            else
            {
                img[y_start + i][x_start + j][0] = brain[z*d_x*d_y + i*d_y + j];
                img[y_start + i][x_start + j][1] = brain[z*d_x*d_y + i*d_y + j];
                img[y_start + i][x_start + j][2] = brain[z*d_x*d_y + i*d_y + j];
            }
        }
    }
}


/*smoothing_10: function to get smoothed images */
void smoothing_10(int d_x, int d_y, vector<vector<my_double> > &slice){
    my_double gauss_window[10][10] = {{0.0019,    0.0066,    0.0167,    0.0310,    0.0423,    0.0423,    0.0310,    0.0167,    0.0066,    0.0019},{0.0066,    0.0228,    0.0576,    0.1067,    0.1453,    0.1453,    0.1067,    0.0576,    0.0228,    0.0066},{0.0167,    0.0576,    0.1453,    0.2694,    0.3667,    0.3667,    0.2694,    0.1453,    0.0576 ,   0.0167},{0.0310,    0.1067,    0.2694,    0.4994,    0.6799,    0.6799,    0.4994,    0.2694,    0.1067 ,   0.0310},{0.0423,    0.1453,    0.3667,    0.6799,    0.9257,    0.9257,    0.6799,    0.3667,    0.1453 ,   0.0423},{0.0423,    0.1453,    0.3667,    0.6799,    0.9257,    0.9257,    0.6799,    0.3667,    0.1453 ,   0.0423},{0.0310,    0.1067,    0.2694,    0.4994,    0.6799,    0.6799,    0.4994,    0.2694,    0.1067 ,   0.0310},{0.0167,    0.0576,    0.1453,    0.2694,    0.3667,    0.3667,   0.2694 ,   0.1453 ,   0.0576  ,  0.0167},{0.0066,    0.0228,    0.0576,    0.1067,    0.1453,    0.1453,   0.1067 ,   0.0576 ,   0.0228  ,  0.0066},{0.0019,    0.0066,    0.0167,    0.0310,    0.0423,    0.0423,   0.0310 ,   0.0167 ,   0.0066  ,  0.0019}};

    my_double sum_window;
    my_double sum;

    int i, j, m, n;
    for(i=0;i<d_y;i++)
    {
        for(j=0;j<d_x;j++)
        {
            sum_window = 0;
            sum = 0;
            for(m=-4;m<5;m++)
            {
                for(n=-4;n<5;n++)
                {
                    if(i+m >= 0 and i+m <d_y and j+n >= 0 and j+n <d_x)
                    {
                        sum += slice[i+m][j+n] * gauss_window[m+4][n+4];
                        sum_window += gauss_window[m+4][n+4];
                    }
                }
            }
            slice[i][j] = sum/sum_window;
        }
    }
}



/*vett_from_mat: function to get a column vector from the input matrix*/
void vett_from_mat(my_double **vett, int r_vett, my_double **mat, int ind){  //ottengo vett colonna
    int i;
    for (i=0;i<r_vett;i++)
        vett[i][0]=mat[i][ind];
}

/*zeros_mat: function to initialize the elements of the input matrix to zero*/
void zeros_mat(my_double **mat, int r, int c) {
    int i,j;

    for (i=0;i<r;i++)
        for(j=0;j<c;j++)
            mat[i][j]=0.0;
}






































































